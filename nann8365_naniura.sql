-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 22, 2018 at 09:45 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nann8365_naniura`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Sushi'),
(2, 'Appetizers'),
(3, 'Soup & Salad'),
(4, 'Sashimi'),
(5, 'Noodles'),
(6, 'Rice Bowl');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `content` text NOT NULL,
  `cover` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `title`, `date`, `content`, `cover`, `status`) VALUES
(100100, 'Bonbon Gelato Buy 1 Get 1', '2018-03-20 08:06:36', 'Great news!!! bonbon having a buy 1 get 1 promotion for the first 100 customers everyday. &nbsp;Don’t miss it.\r\n\r\n                    ', 'assets/img/promo-bonbon.jpg', 1),
(100101, 'Two Hours of All You Can Eat Sushi', '2018-03-25 15:30:35', '<div>The Good News has arrived!&nbsp;</div><div>The All You Can Eat that you\'ve all been waiting for has finally come to reality!&nbsp;</div><div>if you\'re a Naniura Sushibar &amp; Restaurant Member, you can get it for only Rp99K!!&nbsp;</div><div><br></div><div>Terms &amp; Conditions:&nbsp;</div><div>Start from 5pm to 7pm (last entry)&nbsp;</div><div>For member price, 1 member card is valid for 2 persons/day &amp; ID card must be shown&nbsp;</div><div>Children below 5 years old is FREE&nbsp;</div><div>Maximum duration is 2 hours/table&nbsp;</div><div>Cant be combined with other promotions &amp; membership programs&nbsp;</div><div>No reservations&nbsp;</div><div>Dine in only&nbsp;</div><div>Prices are before tax &amp; service&nbsp;</div>\r\n\r\n                    ', 'assets/img/NAN-160006-allyoucan.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `fav` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idcategory` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `cover`, `fav`, `active`, `date`, `idcategory`) VALUES
(8, 'ggsdgsgdsgdsss', '', 1, 2, '2017-12-29 15:00:29', 2),
(9, 'Sashimi Salmon Shrimp', '', 1, 4, '2018-02-12 08:20:49', 4),
(13, 'Salmon Sashimi Salad', 'assets/img/salmon-sashimi-salad.png', 0, 1, '2018-03-14 02:12:58', 3),
(14, 'Mushroom Soup', 'assets/img/mushroom-soup.png', 0, 1, '2018-03-14 02:13:26', 3),
(15, 'Salmon Sashimi', 'assets/img/salmon-sashimi.png', 1, 1, '2018-03-14 14:22:30', 1),
(17, 'Nigiri Sushi', 'assets/img/nigiri-sushi.png', 1, 1, '2018-03-14 14:23:32', 1),
(18, 'Gunkan', 'assets/img/gunkan.png', 1, 1, '2018-03-14 14:24:18', 1),
(21, 'California Roll', 'assets/img/california-roll2.png', 1, 1, '2018-03-14 14:40:53', 1),
(22, 'Scooby Roll', 'assets/img/Scooby-Roll.png', 0, 1, '2018-03-14 14:42:21', 1),
(23, 'Salmon Mentai Roll', 'assets/img/Salmon-Mentai-Roll.png', 0, 1, '2018-03-14 14:43:31', 1),
(24, 'Tintin Roll', 'assets/img/Tintin-Roll.png', 1, 1, '2018-03-14 14:44:47', 1),
(26, 'Original Yakiudon', 'assets/img/Original-Yakiudon.png', 1, 1, '2018-03-14 14:53:55', 5),
(27, 'Beef Ramen', 'assets/img/beef-ramen2.png', 1, 1, '2018-03-14 14:56:30', 5),
(28, 'Mushroom Creme Yakiudon', 'assets/img/Mushroom-Creme-Yakiudon.png', 1, 1, '2018-03-14 14:58:57', 5),
(29, 'Seafood Creme Yakiudon', '', 1, 1, '2018-03-14 14:59:25', 5),
(30, 'Salmon Belly Soup', '', 1, 1, '2018-03-14 15:03:54', 3),
(31, 'Healthy Salad', '', 1, 1, '2018-03-14 15:04:23', 3),
(32, 'Chicken Teriyaki', 'assets/img/Chicken-Teriyaki.png', 1, 1, '2018-03-14 15:06:24', 6),
(33, 'Salmon Teriyaki', '', 1, 1, '2018-03-14 15:07:00', 6),
(34, 'Chicken Furai', 'assets/img/Chicken-Furai.png', 1, 1, '2018-03-14 15:08:02', 6),
(35, 'Beef Furai', '', 1, 1, '2018-03-14 15:08:41', 6),
(36, 'Banana Split', 'assets/img/banana-split.png', 1, 1, '2018-03-14 15:10:40', 4),
(37, 'Strawberry Parfait', 'assets/img/Strawberry-Parfait.png', 1, 1, '2018-03-14 15:11:29', 4),
(38, 'Coffee Oreo', 'assets/img/Coffee-Oreo.png', 1, 1, '2018-03-14 15:12:20', 4),
(39, 'Avocado Juice', 'assets/img/Avocado-Juice.png', 1, 1, '2018-03-14 15:13:18', 4),
(40, 'Enoki Bacon', 'assets/img/enoki-bacon1.png', 1, 1, '2018-03-25 15:03:36', 2),
(41, 'Tempura Mori', '', 0, 1, '2018-03-25 15:04:51', 2),
(42, 'Beef Kushi', 'assets/img/beef-kushi.png', 0, 1, '2018-03-25 15:06:54', 2),
(43, 'Salmon Croquette', '', 1, 1, '2018-03-25 15:07:53', 2),
(44, 'Spicy Salmon Inari', 'assets/img/spicy-salmon-inari.png', 1, 1, '2018-03-25 15:11:09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `cover` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `cover`) VALUES
(6, 'assets/img/Screen_Shot_2018-01-11_at_10_45_08_AM.png'),
(7, 'assets/img/Screen_Shot_2018-01-11_at_10_45_05_AM.png'),
(8, 'assets/img/Screen_Shot_2018-01-11_at_10_45_01_AM.png');

-- --------------------------------------------------------

--
-- Table structure for table `subscribe`
--

CREATE TABLE `subscribe` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribe`
--

INSERT INTO `subscribe` (`id`, `email`) VALUES
(13, '12feb@12feb.com'),
(10, 'wwwwwaaaaa@gasd.cozzzz'),
(12, 'aasda@fjdidc.com'),
(11, 'rb.playground@gmail.com'),
(9, 'wwwwwaaaaa@gasd.co'),
(14, 'test@test.com'),
(15, 'ifiuhieurfheirufh@fiuhgiufhgiufhg.com'),
(16, 'ifiuhieurfheirufh@fiuhgiufhgiufhg.com'),
(17, 'rb.playground@gmail.com'),
(18, 'rendy.tantiono@gmail.com'),
(19, 'rendy.tantiono@gmail.com'),
(20, 'rendy.tantiono@gmail.com'),
(21, 'beatupsoldiers@yahoo.com'),
(22, 'asdjaskjd@sdasdas.com'),
(23, 'asjdasjd@dfskdjf.com'),
(24, 'anggay@gay.com'),
(25, 'adsad@fsdjfskjf.com'),
(26, 'rendy@anal.com'),
(27, 'rendy@anal.com'),
(28, 'rendy@anal.com'),
(29, 'qaqapades@gmail.com'),
(30, 'sapi@sapi.mo'),
(31, 'gay@gay.cm'),
(32, 'agay@gay.com'),
(33, 'adsjahb@jhsbdjshb.com'),
(34, 'ricardobongq@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` bigint(12) UNSIGNED NOT NULL,
  `user_id_fb` bigint(25) DEFAULT NULL,
  `user_email` varchar(255) NOT NULL DEFAULT '',
  `user_pass` varchar(60) NOT NULL DEFAULT '',
  `user_name` varchar(255) NOT NULL,
  `user_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `phone` varchar(15) NOT NULL,
  `birthday` date NOT NULL,
  `point` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL,
  `user_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_last_login` datetime DEFAULT NULL,
  `activate` int(11) NOT NULL DEFAULT '0',
  `admin` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_id_fb`, `user_email`, `user_pass`, `user_name`, `user_date`, `phone`, `birthday`, `point`, `image`, `user_modified`, `user_last_login`, `activate`, `admin`) VALUES
(1, NULL, 'qweqwe', 'qweqweqw', '', '2017-12-29 14:48:55', '', '0000-00-00', 30, '', '2017-12-29 14:48:55', NULL, 0, 0),
(1648372949, NULL, 'admin@naniurasushi.com', '$2a$08$fSvD3gwZfrok9oNF9wXWBOyTdU8Wg.lO3.njE0G9etkz0zQU6sjRa', 'admin', '2017-12-29 12:55:05', '08937282738', '1945-01-01', 253, 'assets/img/profile-picture-admin.jpg', '2018-02-13 03:56:36', '2018-04-12 12:06:54', 1, 1),
(1648372952, 0, 'undefined', '', 'undefined', '2017-12-29 22:39:14', '', '0000-00-00', 0, '', '2017-12-29 22:39:14', NULL, 1, 0),
(1648372954, NULL, 'cashier@naniurasushi.com', '$2a$08$JIHM2O7W9Bd/Fxz5YEKOr.u/JvYJvxmvNbLiKuggWUkq80qQZPGiy', 'Cashier', '2018-01-02 09:38:26', '-', '0000-00-00', 0, '', '2018-01-02 09:38:26', '2018-04-13 20:54:46', 1, 2),
(1648372956, NULL, 'faza@crypemail.info', '$2a$08$PxFGlX7simHBjMAmlNHpj.CTltxPLR/Wuw5U.GEdaMcTuBGVNOdvS', 'Sytel', '2018-01-12 08:43:38', '1383839383939', '1945-10-01', 0, 'assets/img/image.jpg', '2018-01-12 08:43:38', NULL, 0, 0),
(1648372963, 10216350562855804, 'secretbox_rita@yahoo.com', '', 'Rita Siagian', '2018-02-19 14:32:31', '', '0000-00-00', 75228, '', '2018-02-19 14:32:31', NULL, 1, 0),
(1648372969, NULL, 'rendyloui@gmail.com', '$2a$08$2yHFZEDdT.CaVx.C6sWqxuVMmJm5OgKTznD3tiCbKRRQw2r1dGtCG', 'rendy', '2018-02-19 15:08:36', '08973754682', '1945-01-01', 0, '', '2018-02-19 15:08:36', '2018-02-19 15:09:34', 1, 0),
(1648372975, NULL, 'baababa@gmail.com', '$2a$08$uU6kUJc8StFUJCLkil2nN.TCtigR./Hn6EG.7.4NX9uIVgEJNJx5.', 'baa', '2018-02-19 15:39:22', '0897237238', '1945-01-01', 99, '', '2018-02-19 15:39:22', NULL, 0, 0),
(1648372994, 10210013327042669, 'rendy.agus37@yahoo.com', '', 'Rendy Agus Tantiono', '2018-02-25 10:42:17', '1231231244', '1972-11-16', 99, 'assets/img/Screenshot_from_2018-02-19_14-22-21.png', '2018-02-25 10:42:17', NULL, 1, 0),
(1648372995, NULL, 'oqekoweko@gmail.com', '$2a$08$bJngvxd0pi0glb4J2r5J9u1GE0/LB3RoxBA7KTUe52qbBVCM/PauC', 'rendy', '2018-02-25 03:46:12', '081923892', '1945-01-01', 99, 'assets/img/Screenshot_from_2018-02-12_16-37-12.png', '2018-02-25 03:46:12', NULL, 0, 0),
(1648372996, NULL, 'zelut@kumail8.info', '$2a$08$V98ecjXjnRJ2CFjK.Gm5lutH/Wz7PnDsDkmwQcnDRfHt0gLeSoTqi', 'Ajatwkwhwgqj', '2018-02-25 03:53:58', '1527262617277', '1974-05-07', 99, '', '2018-02-25 03:53:58', '2018-02-25 03:56:05', 0, 0),
(1648372997, NULL, 'tomu@0mixmail.info', '$2a$08$ScfiLZpI8OjXC7i8QP5u..LooKDRfJ09/TUaXpiCHEaVYWS9IGK8C', 'Tom Jerry', '2018-02-25 04:04:50', '1234567890', '1960-07-15', 99, 'assets/img/avatar_aing.png', '2018-02-25 04:04:50', '2018-02-25 04:07:30', 1, 0),
(1648373004, NULL, 'bergmannkristina897@gmail.com', '$2a$08$xypYAgAh4Oeii60AQ4JkHuyNl.B/QF9D6YvFvHjY3fO.qUpj4wDE6', 'Kristina bergmann', '2018-02-27 18:46:16', '081230242725', '1988-01-07', 99, '', '2018-02-27 18:46:16', '2018-02-27 19:31:59', 1, 0),
(1648373005, 1870957619583907, 'bergmannkristina@gmail.com', '$2a$08$5Wqy7Sifc0yNKU6VaJfp8unk1ClaM8ofj8T63INutiAiPjTxo./Ay', 'Bergmann Kristina', '2018-02-27 18:48:22', '081230242725', '1998-01-07', 932, '', '2018-02-27 19:17:36', '2018-03-13 13:32:25', 1, 0),
(1648373006, NULL, 'kristinabergmann@yahoo.com', '$2a$08$/x/icPEK7ADSwBTEPg5Ma.Epl07t6yiFP1o4SaqnsQT.A0XBWap.m', 'Kristina', '2018-02-27 19:12:02', '081230242725', '1988-01-07', 99, '', '2018-02-27 19:12:02', NULL, 0, 0),
(1648373008, NULL, 'rendy.tantiono@gmail.com', '$2a$08$Gvg2xfVRNuQUGuuAcxajGubQkIeqnNnt7DxLtlpYNBCjFwIPFOCGq', 'endyR', '2018-02-27 19:28:29', '089375728993', '1945-01-01', 99, '', '2018-03-25 15:19:21', '2018-03-25 15:19:29', 1, 0),
(1648373009, 1997377430480708, 'beatupsoldiers@yahoo.com', '', 'Raditya Hartanto', '2018-02-27 19:39:22', '083876173829', '2002-11-30', 99, 'assets/img/04d04e67-9556-42a0-b1de-4b3c8f53efbe.jpg', '2018-02-27 19:39:22', NULL, 1, 0),
(1648373013, NULL, 'rb.playground@gmail.com', '$2a$08$SjZ8eO7Mtsa4nVkKCrftpue4wcAzsUCrlnV9/Wxjo62jbFgwltitS', 'asdfghjkl', '2018-03-02 23:53:09', '123456789', '1954-08-10', 99, '', '2018-03-02 23:53:09', NULL, 0, 0),
(1648373014, NULL, 'benedica.eden@yahoo.com', '$2a$08$co8CMuDKM5BU9xZmu43s.eiNmw73kPjU9/k2SI.6V/sEybaRNX4Nm', 'Eden Benedica', '2018-03-13 17:46:46', '0818110592', '1982-05-11', 152, '', '2018-03-13 17:46:46', '2018-03-13 17:49:08', 1, 0),
(1648373015, NULL, 'tia.arifin@gmail.com', '$2a$08$ibyK/HpCPC5ol/zhugwavOM05d6VEA0Ve87CPKawEnGxd8Ai3pj0i', 'TIA ABUBAKAR ARIFIN', '2018-03-14 14:54:21', '08129973477', '1977-02-11', 99, '', '2018-03-14 14:54:21', NULL, 1, 0),
(1648373016, NULL, 'aisyahdwipadmi@gmail.com', '$2a$08$ozywpdGsON1ek2PKzICzvuAWy2qX2G5ErPPJ/rXEQXtACZ/V4AceK', 'Aisyah Dwipadmi', '2018-03-18 16:46:28', '087875766992', '1982-02-22', 213, '', '2018-03-18 16:46:28', '2018-03-18 16:48:38', 1, 0),
(1648373017, NULL, 'hg_hendragunawan@yahoo.co.id', '$2a$08$OrOTjSvy1wtEeV50VDNz7e97Zvt9ie8PblEdJv8GIIAnhBKEp4YxO', 'Hendra Ginawan', '2018-03-20 15:38:32', '08129271173', '1976-05-25', 279, 'assets/img/8F79DE92-EEC0-4F47-9A56-DAECA5680096.jpeg', '2018-03-20 15:38:32', '2018-03-20 15:41:20', 1, 0),
(1648373018, NULL, 'udien.jawer29@gmail.com', '$2a$08$aWA87Sc0HYDXyDYrKJZwMuN32SIi429ucJ82o1YJu8U50XsYc8jiq', 'Dina dwi', '2018-03-25 20:06:07', '081284018598', '1983-09-29', 156, '', '2018-03-25 20:06:07', '2018-04-17 17:06:33', 1, 0),
(1648373019, NULL, 'kharismacandrap@gmail.com', '$2a$08$8GoLn4SnDkDJc6PctMbmeeXfPk.VqkjQ76v3kfOk.U5pUjgIs4UaW', 'Kharisma candra p', '2018-03-30 18:25:15', '088211480741', '1986-12-10', 161, '', '2018-03-30 18:25:15', '2018-03-30 18:25:35', 1, 0),
(1648373020, NULL, 'zahrainy1@gmail.com', '$2a$08$QPBXdHD3a0vKzTYrxxxhUud6FjBQMbtsNxdC5RwhNUu9GQQP5qUmq', 'Ainy Zahra', '2018-04-01 21:25:14', '081283333487', '1984-10-09', 371, 'assets/img/E7A7F35D-86BD-4074-AA52-6F824560C8BB.jpeg', '2018-04-01 21:25:14', '2018-04-01 21:29:54', 1, 0),
(1648373021, 1746757102013434, 'ricardobongq@gmail.com', '', 'Ricardo Banda Naira', '2018-04-01 21:29:17', '', '0000-00-00', 99, '', '2018-04-01 21:29:17', NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `point` int(11) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`id`, `title`, `point`, `cover`, `status`) VALUES
(8, 'Free One Scoop', 300, 'assets/img/one-scoop.jpg', 1),
(9, 'Free Double Scoop', 500, 'assets/img/double-scoop.jpg', 1),
(10, 'Free 650ml Gelato to Go', 1000, 'assets/img/gelato-to-go-ok.jpg', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idcategory` (`idcategory`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribe`
--
ALTER TABLE `subscribe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100102;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `subscribe`
--
ALTER TABLE `subscribe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` bigint(12) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1648373022;

--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `fkmenucategory` FOREIGN KEY (`idcategory`) REFERENCES `category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
