-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 14 Des 2017 pada 08.30
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `naniura`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_email` varchar(255) NOT NULL DEFAULT '',
  `user_pass` varchar(60) NOT NULL DEFAULT '',
  `user_name` varchar(255) NOT NULL,
  `user_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `phone` varchar(15) NOT NULL,
  `birthday` date NOT NULL,
  `point` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL,
  `user_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_last_login` datetime DEFAULT NULL,
  `activate` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`user_id`, `user_email`, `user_pass`, `user_name`, `user_date`, `phone`, `birthday`, `point`, `image`, `user_modified`, `user_last_login`, `activate`) VALUES
(1, 'qweqw@qewqe.com', '$2a$08$nLP4uZTmBAQ4tNP0zIuy6u4HxJiGwlryHRvJdVpF5dqmjDNLgnjiW', '', '2017-12-13 17:18:19', 'root', '0000-00-00', 0, '', '2017-12-13 17:18:19', NULL, 0),
(2, 'qweqw@qewqeee.com', '$2a$08$7rt57kAEwP1UIfG6ZCYYGenthqK/4r9fimvnBXqZBPhFk7iyjZ46a', 'qweqwe', '2017-12-13 17:20:23', '081239218', '1945-01-01', 0, '', '2017-12-13 17:20:23', NULL, 0),
(3, 'qweqw@qewqeeeee.com', '$2a$08$0uBSBPGsKvAztR910wsre.ArJryN6IXvv2g5inix3xhSer0iC5wFm', 'qweqwe', '2017-12-13 17:25:29', '081239218', '1945-01-01', 0, '', '2017-12-13 17:25:29', NULL, 0),
(4, 'qweqw@qewqeeeeeee.com', '$2a$08$k2eJRZ87hItsyweD0lHvmOK0cmRO5kM7NYcgq4XxAwZCjOSvWVVRO', 'qweqwe', '2017-12-13 17:26:37', '081239218', '1945-01-01', 0, '', '2017-12-13 17:26:37', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
