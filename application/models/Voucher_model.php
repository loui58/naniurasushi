<?php

class Voucher_model extends CI_Model{

  public function get_all_voucher(){
    $this->db->select("*");
    $this->db->from("voucher");
    return $this->db->get()->result_array();
  }
  public function get_all_voucher_active(){
    $this->db->select("*");
    $this->db->from("voucher");
    $this->db->where("status",1);
    $this->db->order_by("point","asc");
    return $this->db->get()->result_array();
  }
  public function get_one_voucher($id){

		$this->db->where('id', $id);
		//$this->CI->db->where('activate', 1);
		$query = $this->db->get_where('voucher');
		return $query->result_array();
  }
  public function get_all_promo_active(){
    $this->db->select("*");
    $this->db->from("event");
    $this->db->where("status",1);
    return $this->db->get()->result_array();

  }
  public function add_voucher($data){
    $this->db->insert('voucher',$data);
  }
  public function update($data,$id){

    $this->db->where('id', $id);
    $this->db->update('voucher', $data);
  }
}


 ?>
