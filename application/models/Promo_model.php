<?php

class Promo_model extends CI_Model{

  public function get_all_promo(){
    $this->db->select("*");
    $this->db->from("event");
    return $this->db->get()->result_array();
  }
  public function get_one_promo($id){

		$this->db->where('id', $id);
		//$this->CI->db->where('activate', 1);
		$query = $this->db->get_where('event');
		return $query->result_array();
  }
  public function get_all_promo_active(){
    $this->db->select("*");
    $this->db->from("event");
    $this->db->where("status",1);
    $this->db->order_by("id","DESC");
    return $this->db->get()->result_array();

  }
  public function get_other_promo($id){
      $this->db->select("*");
      $this->db->from('event');
      $this->db->where("id!=",$id);
      $this->db->order_by("date","desc");
      $this->db->limit(3);
      return $this->db->get()->result_array();
  }
  public function get_one_latest(){
    $this->db->select("*");
    $this->db->from("event");
    $this->db->where("status",1);
    $this->db->order_by("date","DESC");
    $this->db->limit(5);
    return $this->db->get()->result_array();

  }
  public function add_promo($data){
    $this->db->insert('event',$data);
  }
  public function update_promo($data,$id){

    $this->db->where('id', $id);
    $this->db->update('event', $data);
  }
}


 ?>
