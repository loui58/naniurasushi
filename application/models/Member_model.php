<?php

class Member_model extends CI_Model{

  public function get_one_member($id){

		$this->db->where('user_email', $id);
		//$this->CI->db->where('activate', 1);
		$query = $this->db->get_where('users');
		return $query->row_array();
  }
  public function get_one_member_by_fbid($id){
      
  		$this->db->where('user_id_fb', $id);
  		//$this->CI->db->where('activate', 1);
  		$query = $this->db->get_where('users');
  		return $query->row_array();
      
  }
  public function get_all_member(){
    $this->db->select("*");
    $this->db->from('users');
    return $this->db->get()->result_array();
  }
  public function get_one_member_by_id($id){

  		$this->db->where('user_id', $id);
  		//$this->CI->db->where('activate', 1);
  		$query = $this->db->get_where('users');
  		return $query->row_array();
    }
  public function update($data){

    $this->db->where('user_email', $data['user_email']);
    $this->db->update('users', $data);
  }
  public function update_by_id($data){

    $this->db->where('user_id', $data['user_id']);
    $this->db->update('users', $data);
  }
  public function add_subscriber($data){

    $this->db->insert('subscribe',$data);
  }
}


 ?>
