<?php

class Menu_model extends CI_Model{

  public function get_one_menu($id){

		$this->db->where('id', $id);
		//$this->CI->db->where('activate', 1);
		$query = $this->db->get_where('menu');
		return $query->row_array();
  }
  public function update_menu($data,$id){

    $this->db->where('id', $id);
    return $this->db->update('menu', $data);
  }
  public function get_all_menu(){
    $this->db->select("*,menu.id as id, category.id as idcat, menu.name as menuname,category.name as categoryname");
    $this->db->from("menu");
    $this->db->join("category","menu.idcategory=category.id");
    return $this->db->get()->result_array();
  }
  public function get_all_menu_active(){
    $this->db->select("*");
    $this->db->from("menu");
    $this->db->where("active",1);
    return $this->db->get()->result_array();
  }
  public function get_menu_active_by_category($id){
    $this->db->select("*");
    $this->db->from("menu");
    $this->db->where("active",1);
    $this->db->where("idcategory",$id);
    return $this->db->get()->result_array();
  }
  public function add_menu($data){
    $this->db->insert('menu',$data);
  }
  public function get_four_menu_latest(){

    $this->db->select("*");
    $this->db->from("menu");
    $this->db->where("active",1);
    $this->db->where("fav",1);
    $this->db->order_by('date', 'DESC');
    $this->db->limit(4);
    return $this->db->get()->result_array();
  }
  public function get_category(){
    $this->db->select("*");
    $this->db->from("category");
    return $this->db->get()->result_array();
  }
}


 ?>
