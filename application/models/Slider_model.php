<?php

class Slider_model extends CI_Model{
  public function get_all_slider(){
    $this->db->select("*");
    $this->db->from("slider");
    return $this->db->get()->result_array();
  }
  public function get_one_slider($id){
		$this->db->where('id', $id);
		//$this->CI->db->where('activate', 1);
		$query = $this->db->get_where('slider');
		return $query->result_array();
  }
  public function update($data,$id){
    $this->db->where('id', $id);
    $this->db->update('slider', $data);
  }
  public function add_data($data){
    $this->db->insert('slider',$data);
  }
  public function delete($id){
    $this->db->delete('slider', array('id' => $id)); 
  }

}


 ?>
