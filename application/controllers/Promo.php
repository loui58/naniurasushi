<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->library('SimpleLoginSecure');
    $this->load->model('member_model','member');
    $this->load->model('promo_model','promo');
    $this->load->helper('time');

  }
  public function index(){
      $data['promo']=$this->promo->get_all_promo_active();
      $this->load->view('pages/promo-event',$data);
  }
  public function detail($id){
      $data['promo']=$this->promo->get_one_promo($id);
      $data['promo'][0]['ago']=time_elapsed_string($data['promo'][0]['date']);
      $data['other']=$this->promo->get_other_promo($id);
      $this->load->view('pages/promo-event-detail',$data);
  }
  public function manage(){
     if($this->session->userdata('logged_in')) {
      $member = $this->member->get_one_member($this->session->userdata('user'));
      if($member['admin']!=1){
          redirect('member');
      }
  }else{
      redirect('member');
  }
    $data['promo']=$this->promo->get_all_promo();
    
    $this->load->view('templates/headermanage');
    $this->load->view('content/Order/Manage2',$data);
  }
  public function add(){
      if($this->session->userdata('logged_in')) {
      $member = $this->member->get_one_member($this->session->userdata('user'));
      if($member['admin']!=1){
          redirect('member');
      }
  }else{
      redirect('member');
  }
    if($_POST==null){
      $this->load->view("content/Order/Add");
    }else{
      $src = $_FILES['cover']['tmp_name'];
			$targ = "assets/img/".$_FILES['cover']['name'];
			$name=$_FILES['cover']['name'];
		  move_uploaded_file($src, $targ);
		  $name=str_replace(' ','_',$name);
      $data['cover']='assets/img/'.$name;
      $data['content']=$this->input->post('content');
      $data['title']=$this->input->post('title');
      $data['status']=$this->input->post('status');
      $this->promo->add_promo($data);
      redirect("promo/manage");
    }
  }
  public function edit($id=null){
      if($this->session->userdata('logged_in')) {
      $member = $this->member->get_one_member($this->session->userdata('user'));
      if($member['admin']!=1){
          redirect('member');
      }
  }else{
      redirect('member');
  }
    if($_POST==null){
      $data['promo']=$this->promo->get_one_promo($id);
      $this->load->view("content/Order/Edit",$data);
    }else{
      $this->form_validation->set_rules('title', 'Title', 'required');
      if ($this->form_validation->run() == FALSE){
        $data['promo']=$this->promo->get_one_promo($this->input->post('id'));
        $this->load->view("content/Order/Edit",$data);
      }else{
        if($_FILES!=null){
          $src = $_FILES['cover']['tmp_name'];
    			$targ = "assets/img/".$_FILES['cover']['name'];
    			$name=$_FILES['cover']['name'];
    		  move_uploaded_file($src, $targ);
    		  
		  $name=str_replace(' ','_',$name);
          $data['cover']='assets/img/'.$name;
        }
        $data['content']=$this->input->post('content');
        $data['id']=$this->input->post('id');
        $data['title']=$this->input->post('title');
        $data['status']=$this->input->post('status');
        $this->promo->update_promo($data,$data['id']);
      }
    }
  }

}
