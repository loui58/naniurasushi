<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->library('SimpleLoginSecure');
    $this->load->model('member_model','member');
    $this->load->model('slider_model','slider');

  }
  public function index(){
    $data['slider']=$this->slider->get_all_slider();
    $this->load->view('templates/headermanage');
    $this->load->view('content/Slider/Add',$data);

  }
  public function add(){
       if($this->session->userdata('logged_in')) {
      $member = $this->member->get_one_member($this->session->userdata('user'));
      if($member['admin']!=1){
          redirect('member');
      }
  }else{
      redirect('member');
  }
    $data=$this->slider->get_all_slider();
    if(count($data)>9){
      redirect('slider');
    }
    if($_POST==null){
        $this->load->view('content/Slider/Addslid');
    }else{
        $config = array(
          'upload_path' => "assets/img/",
          'allowed_types' => "gif|jpg|png|jpeg"
        );
    
        $this->load->library('upload', $config);
        if($this->upload->do_upload()){

        $data2['cover'] = 'assets/img/'.$this->upload->data('file_name');
        }
        $this->slider->add_data($data2);
        redirect('slider');
    }
  }
  public function update($id){
 if($this->session->userdata('logged_in')) {
      $member = $this->member->get_one_member($this->session->userdata('user'));
      if($member['admin']!=1){
          redirect('member');
      }
  }else{
      redirect('member');
  }
    $config = array(
      'upload_path' => "assets/img/",
      'allowed_types' => "gif|jpg|png|jpeg"
    );

    $this->load->library('upload', $config);
    if($this->upload->do_upload()){

      $data['cover'] = 'assets/img/'.$this->upload->data('file_name');
    }else{
      redirect('slider');

    }
     $this->slider->update($data,$id);
     redirect('slider');
  }
  public function delete($id){

        
     $this->slider->delete($id);
     redirect('slider');
  }


}
?>
