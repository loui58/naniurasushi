<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->library('SimpleLoginSecure');
    $this->load->model('member_model','member');
    $this->load->model('menu_model','menu');
    $this->load->helper('download');

  }
  public function index(){
    $data['sushi']=$this->menu->get_menu_active_by_category(1);
    $data['appetizer']=$this->menu->get_menu_active_by_category(2);
    $data['soup']=$this->menu->get_menu_active_by_category(3);
    $data['sashimi']=$this->menu->get_menu_active_by_category(4);
    $data['noodles']=$this->menu->get_menu_active_by_category(5);
    $data['rice']=$this->menu->get_menu_active_by_category(6);
    $this->load->view('pages/menu',$data);

  }
  public function manage(){
       if($this->session->userdata('logged_in')) {
      $member = $this->member->get_one_member($this->session->userdata('user'));
      if($member['admin']!=1){
          redirect('member');
      }
  }else{
      redirect('member');
  }
    $data['menu']=$this->menu->get_all_menu();

    $this->load->view('templates/headermanage');
    $this->load->view('content/Menu/Manage2',$data);
  }
  public function add(){
       if($this->session->userdata('logged_in')) {
      $member = $this->member->get_one_member($this->session->userdata('user'));
      if($member['admin']!=1){
          redirect('member');
      }
  }else{
      redirect('member');
  }
    if($_POST==null){
      $data['category']=$this->menu->get_category();
      $this->load->view('content/Menu/Add',$data);
    }else{

      $this->form_validation->set_rules('name', 'Name', 'required');
      if ($this->form_validation->run() == FALSE){
          $data['category']=$this->menu->get_category();
          $this->load->view('content/Menu/Add',$data);
      }else{
        $data['name']=$this->input->post('name');
        $data['fav']=$this->input->post('favorite');
        $data['active']=$this->input->post('active');
        $data['idcategory']=$this->input->post('category');
        $config = array(
          'upload_path' => "assets/img/",
          'allowed_types' => "gif|jpg|png|jpeg"
        );

        $this->load->library('upload', $config);
        if($this->upload->do_upload()){

          $data['cover'] = 'assets/img/'.$this->upload->data('file_name');
        }
        $this->menu->add_menu($data);
        redirect('menu/manage');
      }
    }
  }
  public function edit($id=null){
       if($this->session->userdata('logged_in')) {
      $member = $this->member->get_one_member($this->session->userdata('user'));
      if($member['admin']!=1){
          redirect('member');
      }
  }else{
      redirect('member');
  }
 
    if($_POST==null){
      $data['category']=$this->menu->get_category();
      $data['menu']=$this->menu->get_one_menu($id);
      if($data['menu']==null){
        redirect('menu/manage');
      }
      $this->load->view('content/Menu/Edit',$data);
    }else{

      $this->form_validation->set_rules('name', 'Name', 'required');
      if ($this->form_validation->run() == FALSE){
        $data['category']=$this->menu->get_category();
        $data['menu']=$this->menu->get_one_menu($this->input->post('id'));
        if($data['menu']==null){
          redirect('menu/manage');
        }
        $this->load->view('content/Menu/Edit',$data);
      }else{
        $data['id']=$this->input->post('id');
        $data['name']=$this->input->post('name');
        $data['fav']=$this->input->post('favorite');
        $data['active']=$this->input->post('active');
        if($this->input->post('userfile')==null){
          $data['cover']='';
        }else{
          $config = array(
            'upload_path' => "assets/img/",
            'allowed_types' => "gif|jpg|png|jpeg"
          );

          $this->load->library('upload', $config);
          if($this->upload->do_upload()){

            $data['cover'] = 'assets/img/'.$this->upload->data('file_name');
          }

        }

        $this->menu->update_menu($data,$data['id']);
        redirect('menu/manage');
      }
    }

  }
  
  function download_menu(){
      force_download('assets/download/menu.pdf', NULL);
  }
}
