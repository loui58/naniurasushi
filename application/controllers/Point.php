<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Point extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->library('SimpleLoginSecure');
    $this->load->model('member_model','member');
    $this->load->model('voucher_model','voucher');

  }
  public function index(){
    if($this->session->userdata('logged_in')) {

        $this->load->view('pages/redeem');
    }else{
      redirect('member');
    }
  }
  public function redeem(){

    if($this->session->userdata('logged_in')) {
    $data['voucher']=$this->voucher->get_all_voucher_active();
    $data['asd']=$this->member->get_one_member($this->session->userdata('user'));
    $this->load->view('pages/redeem-not-enough',$data);
    }else{
      redirect('member');
    }
  }
  public function redeem_success(){
    if(!$this->session->userdata('logged_in')) {
      redirect('member');
    }
    $data['point']=$this->member->get_one_member($this->session->userdata('user'));

    $this->load->view('pages/redeem-done',$data);
  }
  public function add($id,$jumlah){
    /*if(!$this->session->userdata('logged_in')) {
      redirect('member');
    }*/
    $member=$this->member->get_one_member_by_id($id);

    if($member!=null){
        $jumlah=floor($jumlah/5000);
      $member['point']+=$jumlah;
      $this->member->update($member);
      echo json_encode("200");
    }else{
      echo json_encode("404");
    }
  }
  public function redeem_point($jum){
    if(!$this->session->userdata('logged_in')) {
      redirect('member');
    }
    $data=$this->member->get_one_member($this->session->userdata('user'));
    $poin = $data['point'];
    if($data['point']-$jum>=0){
      $data['point']=$data['point']-$jum;
      $poin=$data['point'];
      $this->member->update($data);
    }else{
        $poin-=$jum;
    }
    echo json_encode($poin);

  }
  public function manage(){
       if($this->session->userdata('logged_in')) {
      $member = $this->member->get_one_member($this->session->userdata('user'));
      if($member['admin']!=1){
          redirect('member');
      }
  }else{
      redirect('member');
  }
    if(!$this->session->userdata('logged_in')) {
      redirect('member');
    }
      $data['voucher']=$this->voucher->get_all_voucher();

    $this->load->view('templates/headermanage');
      $this->load->view('content/Voucher/Manage2',$data);
  }
  public function add_data(){
       if($this->session->userdata('logged_in')) {
      $member = $this->member->get_one_member($this->session->userdata('user'));
      if($member['admin']!=1){
          redirect('member');
      }
  }else{
      redirect('member');
  }
    if($_POST==null){
      $this->load->view('content/Voucher/Add');
    }else{
      $data['title']=$this->input->post("name");
      $data['point']=$this->input->post("point");
      $data['status']=$this->input->post("active");
      $config = array(
        'upload_path' => "assets/img/",
        'allowed_types' => "gif|jpg|png|jpeg"
      );

      $this->load->library('upload', $config);
      if($this->upload->do_upload()){

        $data['cover'] = 'assets/img/'.$this->upload->data('file_name');
      }
      $this->voucher->add_voucher($data);
      redirect("voucher/manage");

    }
  }
  public function edit($id=null){
       if($this->session->userdata('logged_in')) {
      $member = $this->member->get_one_member($this->session->userdata('user'));
      if($member['admin']!=1){
          redirect('member');
      }
  }else{
      redirect('member');
  }
    if($_POST==null){
      $data['voucher']=$this->voucher->get_one_voucher($id);
      if($data==null){
        redirect('voucher/manage');
      }
      $this->load->view('content/Voucher/Edit',$data);
    }else{
      $data['id']=$this->input->post("id");
      $data['title']=$this->input->post("name");
      $data['point']=$this->input->post("point");
      $data['status']=$this->input->post("active");
      $this->voucher->update($data,$data['id']);
      redirect("voucher/manage");

    }
  }

  public function add_point_admin(){
      if($this->session->userdata('logged_in')) {
          $member = $this->member->get_one_member($this->session->userdata('user'));
          if($member['admin']!=2){
              redirect('member');
          }else{
            $this->load->view('pages/admin_redeem');
              
              
          }
      }else{
          redirect('member');
      }
      
}
}
