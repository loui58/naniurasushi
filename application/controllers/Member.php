<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->library('SimpleLoginSecure');
    $this->load->model('member_model','member');

    $this->load->library('form_validation');
  }
  public function index(){
    if($this->session->userdata('logged_in')) {
      redirect('profile');
    }else{
        $data['test']=0;
        if($this->session->flashdata('wrong')!=null){
            
            $data['wrong']=1;
        }
      //$this->load->view('template/header');
      $this->load->view('pages/member',$data);
    }
  }
  public function loginfb(){
      $data=$this->member->get_one_member_by_fbid($this->input->get('id'));
      
      if($data!=null){
        $user_data['user'] = $data['user_email'];
        
		$user_data['role']=$data['admin'];
		$user_data['logged_in'] = true;
		$this->session->set_userdata($user_data);
        echo 'login';
      }else{
        $data['user_id_fb']=$this->input->get('id');
        $data['user_name']=$this->input->get('name');
        $data['user_email']=$this->input->get('email');
        if($this->input->get('birthday')!=null){
        $data['birthday']=$this->input->get('birthday');
        }
        $data['point']=99;
        $data['activate']=1;
        $data['admin']=0;
        
        $user_data['user'] = $this->input->get('email');
		$user_data['role']=0;
		$user_data['logged_in'] = true;
		$this->session->set_userdata($user_data);
        $this->simpleloginsecure->create_by_fb($data);
        echo 'daftar';
      }
      
  }
  public function forgot($id=null){
    if($id==null){
      if($_POST==null){
        $this->load->view('pages/forget-bff');
      }else{
        $member=$this->member->get_one_member($this->input->post('email'));
        if($member!=null){
          $config = Array(
      			'protocol' => 'smtp',
      			'smtp_host' => 'mail.naniurasushi.com',
      			'smtp_port' => 587,
      			'smtp_user' => 'admin@naniurasushi.com',
      			'smtp_pass' => 'downtownlux123',
      			'smtp_timeout' => '4',
      			'mailtype' => 'html',
      			'charset' => 'iso-8859-1'
      		);
          $this->load->library('email', $config);
          $this->email->set_newline("\r\n");
          $this->email->from('admin@naniurasushi.com', 'Naniura');
          $member=$this->member->get_one_member($this->input->post('email'));
          $data['member']=$member;
          $this->email->to($member['user_email']); // replace it with receiver mail id
          $this->email->subject("Reset Password Naniura Membership"); // replace it with relevant subject
          $body=$this->load->view('pages/emailforget',$data,TRUE);
          $this->email->message($body);
          $this->email->send();
          $this->load->view('pages/forgot-done');
        }else{
          $this->load->view('pages/forgot-err');
        }
      }
    }else{

      $data['member']=$this->member->get_one_member_by_id($id);
      $this->load->view('pages/forgot',$data);
    }
  }
  public function forgotconf(){
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('repassword', 'Password Confirmation', 'required|matches[password]');
    if ($this->form_validation->run() == FALSE){

      $data['member']=$this->member->get_one_member_by_id($this->input->post('id'));
      $this->load->view('pages/forgot',$data);
    }else{
      $data['user_pass']=$this->input->post('password');
      $data['user_id']=$this->input->post('id');
      $te=$this->simpleloginsecure->edit_password($this->input->post('id'),$this->input->post('password'));
     
     redirect('member');
    }
  }
  public function register(){
    if($this->session->userdata('logged_in')) {
      redirect('profile');
    }
    if($_POST!=null){
      $tanggal= strtotime($this->input->post('month').'/'.$this->input->post('day').'/'.$this->input->post('year'));
      $newformat = date('Y-m-d',$tanggal);
      $this->form_validation->set_rules('email', 'Email', 'required|is_unique[users.user_email]');
      if ($this->form_validation->run() == FALSE){

        $this->load->view('pages/register');
      }else{
        $data['user_name']=$this->input->post('name');
        $data['user_email']=$this->input->post('email');
        $data['user_pass']=$this->input->post('psw');
        $data['birthday']=$newformat;
        $user['activate']=0;
        $user['admin']=0;
        $user['point']=99;
        $data['phone']=$this->input->post('phone');
        $config['upload_path']          = './assets/img/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('userfile'))
        {
        }else{
          $data['image']='assets/img/'.$this->upload->data('file_name');

        }
        $validator=$this->simpleloginsecure->create($data);
        if($validator){

          $config = Array(
      			'protocol' => 'smtp',
      			'smtp_host' => 'mail.naniurasushi.com',
      			'smtp_port' => 587,
      			'smtp_user' => 'admin@naniurasushi.com',
      			'smtp_pass' => 'downtownlux123',
      			'smtp_timeout' => '4',
      			'mailtype' => 'html',
      			'charset' => 'iso-8859-1'
      		);
      		$this->load->library('email', $config);
      		$this->email->set_newline("\r\n");
      		$this->email->from('admin@naniurasushi.com', 'Naniura');
          $member=$this->member->get_one_member($data['user_email']);
          $data['member']=$member;
      		$this->email->to($member['user_email']); // replace it with receiver mail id
      		$this->email->subject("Confirmation Naniura Membership"); // replace it with relevant subject
      		$body=$this->load->view('pages/email',$data,TRUE);
      		$this->email->message($body);
      	 $this->email->send();
          $this->load->view('template/header');
          $this->load->view('pages/register-done');
        }
      }

    }else {
      $this->load->view('pages/register');
    }


  }
  public function manage(){
  if($this->session->userdata('logged_in')) {
      $member = $this->member->get_one_member($this->session->userdata('user'));
      if($member['admin']!=1){
          redirect('member');
      }
  }else{
      redirect('member');
  }
    $data['member']=$this->member->get_all_member();

    $this->load->view('templates/headermanage',$data);
    $this->load->view('content/Member/Manage2',$data);
    
  }
  public function login(){
    if($_POST!=null){
      if($this->simpleloginsecure->login($this->input->post('email'),$this->input->post('psw'))){
        $data=$this->member->get_one_member($this->session->userdata('user'));
        if($data['activate']==0){
          $data['notactive']=1;

            $this->simpleloginsecure->logout();
          $this->load->view('pages/member',$data);
        }else{
        if($data['admin']==1){
          redirect('promo/manage');
        }else if($data['admin']==0){
          redirect('profile');
        }else{
            redirect('point/add_point_admin');
        }}
      }else {
        $this->session->set_flashdata('wrong', '1');

        redirect('member');
      }
    }
  }
  public function add(){
      if($this->session->userdata('logged_in')) {
      $member = $this->member->get_one_member($this->session->userdata('user'));
      if($member['admin']!=1){
          redirect('member');
      }
  }else{
      redirect('member');
  }
    if($_POST == null){
      $this->load->view('content/Member/Add');
    }else{

        $data['user_name']=$this->input->post('name');
        $data['user_email']=$this->input->post('email');
        $data['user_pass']=$this->input->post('password');
        $data['activate']=$this->input->post('active');
        $data['admin']=$this->input->post('admin');
        $data['phone']='-';
        $data['birthday']='';
        $validator=$this->simpleloginsecure->create($data);
        redirect("member/manage");
    }
  }
  public function edit($id=null){
       if($this->session->userdata('logged_in')) {
      $member = $this->member->get_one_member($this->session->userdata('user'));
      if($member['admin']!=1){
          redirect('member');
      }
  }else{
      redirect('member');
  }
    if($_POST==null){
      $data['member']=$this->member->get_one_member_by_id($id);
      $this->load->view('content/Member/Edit',$data);
    }else{
      $data['user_id']=$this->input->post('id');
      $data['user_email']=$this->input->post('email');
      $data['activate']=$this->input->post('active');
      $data['point']=$this->input->post('point');
      $data['admin']=$this->input->post('admin');
      $this->member->update_by_id($data);
      redirect('member/manage');
    }
  }
  public function edit_profile(){
    if($_POST!=null){
     

          $config['upload_path']          = './assets/img/';
          $config['allowed_types']        = 'gif|jpg|png|jpeg';

          $this->load->library('upload', $config);

          if ( $this->upload->do_upload('userfile'))
          {
              $data['image']='assets/img/'.$this->upload->data('file_name');
            //redirect('member');
          }
          if($this->input->post("psw")!=""){
          $this->simpleloginsecure->edit_password($this->input->post('email'),$this->input->post("psw"));
          }
          if($this->input->post('day')!=''){
          $tanggal= strtotime($this->input->post('month').'/'.$this->input->post('day').'/'.$this->input->post('year'));
      $newformat = date('Y-m-d',$tanggal);
            $data['birthday']=$newformat;
          }
            $data['user_email']=$this->input->post('email');
            $data['phone']=$this->input->post("phone");
            $this->member->update($data);
            redirect('member');
    }else{
      $data['member']=$this->member->get_one_member($this->session->userdata('user'));
      $this->load->view('pages/edit-profile',$data);
    }
  }
  public function profile(){
    if(!$this->session->userdata('logged_in')) {
      redirect('member');
    }
    if(true){
      $data['member']=$this->member->get_one_member($this->session->userdata('user'));
      $this->load->view('pages/profile',$data);
    }
  }
  public function logout(){
    $this->simpleloginsecure->logout();
    redirect('member');
  }
  public function set_barcode()
  {
    $member=$this->member->get_one_member($this->session->userdata('user'));

        //load library
    $this->load->library('zend');
    //load in folder Zend
    $this->zend->load('Zend/Barcode');

    //generate barcode
    Zend_Barcode::render('code128', 'image', array('text'=>$member['user_id']), array());
  }
  public function check_member($id){
    if($this->member->get_one_member_by_id($id)!=null){
      echo json_encode("1");
    }else{
      echo json_encode("0");
    }
  }
  public function activate($id){
    $data['user_id']=$id;
    $data['activate']=1;
    $this->member->update_by_id($data);
    redirect('member');
  }
  public function subscribe(){
    $data['email']=$this->input->post('email');
    $this->member->add_subscriber($data);
    redirect('welcome');
  }
  public function mailus(){
    $data['name']=$this->input->post('name');
    $data['email']=$this->input->post('email');
    $data['question']=$this->input->post('message');
    $config = Array(
      			'protocol' => 'smtp',
      			'smtp_host' => 'mail.naniurasushi.com',
      			'smtp_port' => 587,
      			'smtp_user' => 'admin@naniurasushi.com',
      			'smtp_pass' => 'downtownlux123',
      			'smtp_timeout' => '4',
      			'mailtype' => 'html',
      			'charset' => 'iso-8859-1'
      		);
  	$this->load->library('email', $config);
  	$this->email->set_newline("\r\n");
  	$this->email->from('admin@naniurasushi.com', 'Naniura');
  	$this->email->to('contact@naniurasushi.com'); // replace it with receiver mail id
  	$this->email->subject("Question Naniura"); // replace it with relevant subject
  	$body=$this->load->view('pages/emailqty',$data,TRUE);
  	$this->email->message($body);
     $this->email->send();
      $this->load->view('pages/location-done',$data);
    
  } 
}
