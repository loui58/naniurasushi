<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('slider_model','slider');
		$this->load->model('menu_model','menu');
		$this->load->model('promo_model','promo');

	}
	public function index(){
		$data['slider']=$this->slider->get_all_slider();
		$data['menu']=$this->menu->get_four_menu_latest();
		$data['promo']=$this->promo->get_one_latest();
		$patt = array('@<script[^>]*?>.*?</script>@si','@<[\\/\\!]*?[^<>]*?>@si','@<style[^>]*?>.*?</style>@siU','@<![\\s\\S]*?--[ \\t\\n\\r]*>@');
		$data['promo'][0]['content']=preg_replace($patt, '', $data['promo'][0]['content']);
		$this->load->view('template/header');
		$this->load->view('pages/index',$data);
	}
	public function faq(){
		$this->load->view('pages/faq');
	}
	public function menu(){
		$this->load->view('pages/menu');

	}
	public function bonbon(){
		$this->load->view('pages/bonbon');

	}
	public function location(){
		$this->load->view('pages/location');

	}
	public function shop(){
		$this->load->view('pages/shop');

	}
}
