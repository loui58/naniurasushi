

  <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Member</h1>
      <ol class="breadcrumb">
        <li>Home</li>
        <li><a href="javascript:void(0)">Member</a></li>
      </ol>
    </div>
    <div class="page-content">
      <!-- Panel Basic -->
      <div class="panel">
        <header class="panel-heading">
          <div class="panel-actions"></div>
          <h3 class="panel-title">Manage Member</h3>
          <div class="row" style="margin-left:15px">
            <div class="col-sm-6">
              <div class="margin-bottom-15">
                <a href="add" class="btn btn-primary" type="button">
                  <i class="icon md-plus" aria-hidden="true"></i> Add Data
                </a>
              </div>
            </div>
        </header>
        <div class="panel-body">

          <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>birthday</th>
                <th>Point</th>
                <th>Status</th>
                <th>Role</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              <?php $no=1;foreach($member as $row):?>
              <tr>
                <td><?= $no?></td>
                <td><?=
                  $row['user_name'];
                  ?>
                </td>
                <td><?=
                  $row['user_email'];
                  ?>
                </td>
                <td>
                <?=
                    $row['birthday'];
                    ?>
                  </td>
                <td><?=
                  $row['point'];
                  ?>
                </td>
                <td><?php
                  if($row['activate']==1){
                    echo 'Active';
                  }else{
                    echo 'NotActive';
                  }?>
                </td>

                <td><?php
                  if($row['admin']==1){
                    echo 'Admin';
                  }else if($row['admin']==0){
                    echo 'Member';
                  }else{
                    echo 'Cashier';
                  }?>
                </td>
                <td><a href="<?=base_url();?>member/edit/<?= $row['user_id'];?>" class="btn btn-danger">Edit</a></td>
              </tr>
            <?php $no++;endforeach;?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- End Panel Basic -->
    </div>
  </div>
  <!-- End Page -->


  <!-- Footer -->
  <footer class="site-footer">
    <div class="site-footer-legal">© 2017 Naniura</a></div>
  
  </footer>
  <!-- Core  -->
  <script src="<?= base_url();?>assets/global/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/animsition/animsition.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asscroll/jquery-asScroll.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/mousewheel/jquery.mousewheel.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asscrollable/jquery.asScrollable.all.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/ashoverscroll/jquery-asHoverScroll.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/waves/waves.min.js"></script>

  <!-- Plugins -->
  <script src="<?= base_url();?>assets/global/vendor/switchery/switchery.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/intro-js/intro.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/screenfull/screenfull.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/slidepanel/jquery-slidePanel.min.js"></script>

  <!-- Plugins For This Page -->
  <script src="<?= base_url();?>assets/global/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/datatables-responsive/dataTables.responsive.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asrange/jquery-asRange.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/bootbox/bootbox.js"></script>

  <!-- Scripts -->
  <script src="<?= base_url();?>assets/global/js/core.min.js"></script>
  <script src="<?= base_url();?>assets//js/site.min.js"></script>

  <script src="<?= base_url();?>assets//js/sections/menu.min.js"></script>
  <script src="<?= base_url();?>assets//js/sections/menubar.min.js"></script>
  <script src="<?= base_url();?>assets//js/sections/gridmenu.min.js"></script>
  <script src="<?= base_url();?>assets//js/sections/sidebar.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/configs/config-colors.min.js"></script>
  <script src="<?= base_url();?>assets//js/configs/config-tour.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/components/asscrollable.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/animsition.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/slidepanel.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/switchery.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/tabs.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/components/datatables.min.js"></script>


  <script src="<?= base_url();?>assets//examples/js/tables/datatable.min.js"></script>
  <script src="<?= base_url();?>assets//examples/js/uikit/icon.min.js"></script>


  <!-- Google Analytics -->
  <script>
    (function(i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments)
      }, i[r].l = 1 * new Date();
      a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '<?= base_url();?>assets/<?= base_url();?>assets/<?= base_url();?>assets/<?= base_url();?>assets/www.google-analytics.com/analytics.js',
      'ga');

    ga('create', 'UA-65522665-1', 'auto');
    ga('send', 'pageview');
  </script>
</body>


<!-- Mirrored from getbootstrapadmin.com/remark/material/base/tables/datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Nov 2017 08:37:40 GMT -->
</html>
