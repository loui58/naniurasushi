<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<!-- Mirrored from getbootstrapadmin.com/remark/material/base/forms/editor-summernote.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Nov 2017 08:37:13 GMT -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">

  <title>Summernote | Remark Material Admin Template</title>

  <link rel="apple-touch-icon" href="<?= base_url();?>assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="<?= base_url();?>assets/images/favicon.ico">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/css/bootstrap.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/css/bootstrap-extend.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/css/site.min3f0d.css?v2.2.0">

  <!-- Skin tools (demo site only) -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/css/skintools.min3f0d.css?v2.2.0">
  <script src="<?= base_url();?>assets/js/sections/skintools.min.js"></script>

  <!-- Plugins -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/animsition/animsition.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/asscrollable/asScrollable.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/switchery/switchery.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/intro-js/introjs.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/slidepanel/slidePanel.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/flag-icon-css/flag-icon.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/waves/waves.min3f0d.css?v2.2.0">

  <!-- Plugins For This Page -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/blueimp-file-upload/jquery.fileupload.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/dropify/dropify.min3f0d.css?v2.2.0">
      <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">

  <!-- Fonts -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/fonts/material-design/material-design.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/fonts/brand-icons/brand-icons.min3f0d.css?v2.2.0">

  <link rel='stylesheet' href='<?= base_url();?>assets/<?= base_url();?>assets/<?= base_url();?>assets/fonts.googleapis.com/css5478.css?family=Roboto:400,400italic,700'>

  <link rel="stylesheet" href="<?= base_url();?>assets/global/fonts/font-awesome/font-awesome.min3f0d.css?v2.2.0">

  <!--[if lt IE 9]>
    <script src="<?= base_url();?>assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->

  <!--[if lt IE 10]>
    <script src="<?= base_url();?>assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="<?= base_url();?>assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->

  <!-- Scripts -->
  <script src="<?= base_url();?>assets/global/vendor/modernizr/modernizr.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/breakpoints/breakpoints.min.js"></script>
  <script>
    Breakpoints();
  </script>
</head>
<body>
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

  <nav class="site-navbar navbar navbar-inverse navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
      data-toggle="menubar">
        <span class="sr-only">Toggle navigation</span>
        <span class="hamburger-bar"></span>
      </button>
      <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
      data-toggle="collapse">
        <i class="icon md-more" aria-hidden="true"></i>
      </button>
      <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
        <img class="navbar-brand-logo" src="<?= base_url();?>assets/images/logo.png" title="Remark">
        <span class="navbar-brand-text hidden-xs"> Remark</span>
      </div>
      <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-search"
      data-toggle="collapse">
        <span class="sr-only">Toggle Search</span>
        <i class="icon md-search" aria-hidden="true"></i>
      </button>
    </div>

    <div class="navbar-container container-fluid">
      <!-- Navbar Collapse -->
      <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
        <!-- Navbar Toolbar -->
        <ul class="nav navbar-toolbar">
          <li class="hidden-float" id="toggleMenubar">
            <a data-toggle="menubar" href="#" role="button">
              <i class="icon hamburger hamburger-arrow-left">
                  <span class="sr-only">Toggle menubar</span>
                  <span class="hamburger-bar"></span>
                </i>
            </a>
          </li>
          <li class="hidden-xs" id="toggleFullscreen">
            <a class="icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
              <span class="sr-only">Toggle fullscreen</span>
            </a>
          </li>
          <li class="hidden-float">
            <a class="icon md-search" data-toggle="collapse" href="#" data-target="#site-navbar-search"
            role="button">
              <span class="sr-only">Toggle Search</span>
            </a>
          </li>
          <li class="dropdown dropdown-fw dropdown-mega">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"
            data-animation="fade" role="button">Mega <i class="icon md-chevron-down" aria-hidden="true"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li role="presentation">
                <div class="mega-content">
                  <div class="row">
                    <div class="col-sm-4">
                      <h5>UI Kit</h5>
                      <ul class="blocks-2">
                        <li class="mega-menu margin-0">
                          <ul class="list-icons">
                            <li><i class="md-chevron-right" aria-hidden="true"></i>
                              <a
                              href="<?= base_url();?>assets/advanced/animation.html">Animation</a>
                            </li>data-plugin="summernote"
                            <li><i class="md-chevron-right" aria-hidden="true"></i>
                              <a
                              href="<?= base_url();?>assets/uikit/buttons.html">Buttons</a>
                            </li>
                            <li><i class="md-chevron-right" aria-hidden="true"></i>
                              <a
                              href="<?= base_url();?>assets/uikit/colors.html">Colors</a>
                            </li>
                            <li><i class="md-chevron-right" aria-hidden="true"></i>
                              <a
                              href="<?= base_url();?>assets/uikit/dropdowns.html">Dropdowns</a>
                            </li>
                            <li><i class="md-chevron-right" aria-hidden="true"></i>
                              <a
                              href="<?= base_url();?>assets/uikit/icons.html">Icons</a>
                            </li>
                            <li><i class="md-chevron-right" aria-hidden="true"></i>
                              <a
                              href="<?= base_url();?>assets/advanced/lightbox.html">Lightbox</a>
                            </li>
                          </ul>
                        </li>
                        <li class="mega-menu margin-0">
                          <ul class="list-icons">
                            <li><i class="md-chevron-right" aria-hidden="true"></i>
                              <a
                              href="<?= base_url();?>assets/uikit/modals.html">Modals</a>
                            </li>
                            <li><i class="md-chevron-right" aria-hidden="true"></i>
                              <a
                              href="<?= base_url();?>assets/uikit/panel-structure.html">Panels</a>
                            </li>
                            <li><i class="md-chevron-right" aria-hidden="true"></i>
                              <a
                              href="<?= base_url();?>assets/structure/overlay.html">Overlay</a>
                            </li>
                            <li><i class="md-chevron-right" aria-hidden="true"></i>
                              <a
                              href="<?= base_url();?>assets/uikit/tooltip-popover.html">Tooltips</a>
                            </li>
                            <li><i class="md-chevron-right" aria-hidden="true"></i>
                              <a
                              href="<?= base_url();?>assets/advanced/scrollable.html">Scrollable</a>
                            </li>
                            <li><i class="md-chevron-right" aria-hidden="true"></i>
                              <a
                              href="<?= base_url();?>assets/uikit/typography.html">Typography</a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </div>
                    <div class="col-sm-4">
                      <h5>Media
                        <span class="badge badge-success">4</span>
                      </h5>
                      <ul class="blocks-3">
                        <li>
                          <a class="thumbnail margin-0" href="javascript:void(0)">
                            <img class="width-full" src="<?= base_url();?>assets/global/photos/view-1-150x100.jpg" alt="..."
                            />
                          </a>
                        </li>
                        <li>
                          <a class="thumbnail margin-0" href="javascript:void(0)">
                            <img class="width-full" src="<?= base_url();?>assets/global/photos/view-2-150x100.jpg" alt="..."
                            />
                          </a>
                        </li>
                        <li>
                          <a class="thumbnail margin-0" href="javascript:void(0)">
                            <img class="width-full" src="<?= base_url();?>assets/global/photos/view-3-150x100.jpg" alt="..."
                            />
                          </a>
                        </li>
                        <li>
                          <a class="thumbnail margin-0" href="javascript:void(0)">
                            <img class="width-full" src="<?= base_url();?>assets/global/photos/view-4-150x100.jpg" alt="..."
                            />
                          </a>
                        </li>
                        <li>
                          <a class="thumbnail margin-0" href="javascript:void(0)">
                            <img class="width-full" src="<?= base_url();?>assets/global/photos/view-5-150x100.jpg" alt="..."
                            />
                          </a>
                        </li>
                        <li>
                          <a class="thumbnail margin-0" href="javascript:void(0)">
                            <img class="width-full" src="<?= base_url();?>assets/global/photos/view-6-150x100.jpg" alt="..."
                            />
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div class="col-sm-4">
                      <h5 class="margin-bottom-0">Accordion</h5>
                      <!-- Accordion -->
                      <div class="panel-group panel-group-simple" id="siteMegaAccordion" aria-multiselectable="true"
                      role="tablist">
                        <div class="panel">
                          <div class="panel-heading" id="siteMegaAccordionHeadingOne" role="tab">
                            <a class="panel-title" data-toggle="collapse" href="#siteMegaCollapseOne" data-parent="#siteMegaAccordion"
                            aria-expanded="false" aria-controls="siteMegaCollapseOne">
                                Collapsible Group Item #1
                              </a>
                          </div>
                          <div class="panel-collapse collapse" id="siteMegaCollapseOne" aria-labelledby="siteMegaAccordionHeadingOne"
                          role="tabpanel">
                            <div class="panel-body">
                              De moveat laudatur vestra parum doloribus labitur sentire partes, eripuit praesenti
                              congressus ostendit alienae, voluptati ornateque
                              accusamus clamat reperietur convicia albucius.
                            </div>
                          </div>
                        </div>
                        <div class="panel">
                          <div class="panel-heading" id="siteMegaAccordionHeadingTwo" role="tab">
                            <a class="panel-title collapsed" data-toggle="collapse" href="#siteMegaCollapseTwo"
                            data-parent="#siteMegaAccordion" aria-expanded="false"
                            aria-controls="siteMegaCollapseTwo">
                                Collapsible Group Item #2
                              </a>
                          </div>
                          <div class="panel-collapse collapse" id="siteMegaCollapseTwo" aria-labelledby="siteMegaAccordionHeadingTwo"
                          role="tabpanel">
                            <div class="panel-body">
                              Praestabiliorem. Pellat excruciant legantur ullum leniter vacare foris voluptate
                              loco ignavi, credo videretur multoque choro fatemur
                              mortis animus adoptionem, bello statuat expediunt
                              naturales.
                            </div>
                          </div>
                        </div>

                        <div class="panel">
                          <div class="panel-heading" id="siteMegaAccordionHeadingThree" role="tab">
                            <a class="panel-title collapsed" data-toggle="collapse" href="#siteMegaCollapseThree"
                            data-parent="#siteMegaAccordion" aria-expanded="false"
                            aria-controls="siteMegaCollapseThree">
                                Collapsible Group Item #3
                              </a>
                          </div>
                          <div class="panel-collapse collapse" id="siteMegaCollapseThree" aria-labelledby="siteMegaAccordionHeadingThree"
                          role="tabpanel">
                            <div class="panel-body">
                              Horum, antiquitate perciperet d conspectum locus obruamus animumque perspici probabis
                              suscipere. Desiderat magnum, contenta poena desiderant
                              concederetur menandri damna disputandum corporum.
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- End Accordion -->
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </li>
        </ul>
        <!-- End Navbar Toolbar -->

        <!-- Navbar Toolbar Right -->
        <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)" data-animation="scale-up"
            aria-expanded="false" role="button">
              <span class="flag-icon flag-icon-us"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li role="presentation">
                <a href="javascript:void(0)" role="menuitem">
                  <span class="flag-icon flag-icon-gb"></span> English</a>
              </li>
              <li role="presentation">
                <a href="javascript:void(0)" role="menuitem">
                  <span class="flag-icon flag-icon-fr"></span> French</a>
              </li>
              <li role="presentation">
                <a href="javascript:void(0)" role="menuitem">
                  <span class="flag-icon flag-icon-cn"></span> Chinese</a>
              </li>
              <li role="presentation">
                <a href="javascript:void(0)" role="menuitem">
                  <span class="flag-icon flag-icon-de"></span> German</a>
              </li>
              <li role="presentation">
                <a href="javascript:void(0)" role="menuitem">
                  <span class="flag-icon flag-icon-nl"></span> Dutch</a>
              </li>
            </ul>
          </li>
          <li class="dropdown">
            <a class="navbar-avatar dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"
            data-animation="scale-up" role="button">
              <span class="avatar avatar-online">
                <img src="<?= base_url();?>assets/global/portraits/5.jpg" alt="...">
                <i></i>
              </span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li role="presentation">
                <a href="javascript:void(0)" role="menuitem"><i class="icon md-account" aria-hidden="true"></i> Profile</a>
              </li>
              <li role="presentation">
                <a href="javascript:void(0)" role="menuitem"><i class="icon md-card" aria-hidden="true"></i> Billing</a>
              </li>
              <li role="presentation">
                <a href="javascript:void(0)" role="menuitem"><i class="icon md-settings" aria-hidden="true"></i> Settings</a>
              </li>
              <li class="divider" role="presentation"></li>
              <li role="presentation">
                <a href="javascript:void(0)" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Logout</a>
              </li>
            </ul>
          </li>
          <li class="dropdown">
            <a data-toggle="dropdown" href="javascript:void(0)" title="Notifications" aria-expanded="false"
            data-animation="scale-up" role="button">
              <i class="icon md-notifications" aria-hidden="true"></i>
              <span class="badge badge-danger up">5</span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
              <li class="dropdown-menu-header" role="presentation">
                <h5>NOTIFICATIONS</h5>
                <span class="label label-round label-danger">New 5</span>
              </li>

              <li class="list-group" role="presentation">
                <div data-role="container">
                  <div data-role="content">
                    <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                      <div class="media">
                        <div class="media-left padding-right-10">
                          <i class="icon md-receipt bg-red-600 white icon-circle" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">A new order has been placed</h6>
                          <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">5 hours ago</time>
                        </div>
                      </div>
                    </a>
                    <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                      <div class="media">
                        <div class="media-left padding-right-10">
                          <i class="icon md-account bg-green-600 white icon-circle" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">Completed the task</h6>
                          <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">2 days ago</time>
                        </div>
                      </div>
                    </a>
                    <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                      <div class="media">
                        <div class="media-left padding-right-10">
                          <i class="icon md-settings bg-red-600 white icon-circle" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">Settings updated</h6>
                          <time class="media-meta" datetime="2015-06-11T14:05:00+08:00">2 days ago</time>
                        </div>
                      </div>
                    </a>
                    <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                      <div class="media">
                        <div class="media-left padding-right-10">
                          <i class="icon md-calendar bg-blue-600 white icon-circle" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">Event started</h6>
                          <time class="media-meta" datetime="2015-06-10T13:50:18+08:00">3 days ago</time>
                        </div>
                      </div>
                    </a>
                    <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                      <div class="media">
                        <div class="media-left padding-right-10">
                          <i class="icon md-comment bg-orange-600 white icon-circle" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">Message received</h6>
                          <time class="media-meta" datetime="2015-06-10T12:34:48+08:00">3 days ago</time>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
              </li>
              <li class="dropdown-menu-footer" role="presentation">
                <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button">
                  <i class="icon md-settings" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0)" role="menuitem">
                    All notifications
                  </a>
              </li>
            </ul>
          </li>
          <li class="dropdown">
            <a data-toggle="dropdown" href="javascript:void(0)" title="Messages" aria-expanded="false"
            data-animation="scale-up" role="button">
              <i class="icon md-email" aria-hidden="true"></i>
              <span class="badge badge-info up">3</span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
              <li class="dropdown-menu-header" role="presentation">
                <h5>MESSAGES</h5>
                <span class="label label-round label-info">New 3</span>
              </li>

              <li class="list-group" role="presentation">
                <div data-role="container">
                  <div data-role="content">
                    <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                      <div class="media">
                        <div class="media-left padding-right-10">
                          <span class="avatar avatar-sm avatar-online">
                            <img src="<?= base_url();?>assets/global/portraits/2.jpg" alt="..." />
                            <i></i>
                          </span>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">Mary Adams</h6>
                          <div class="media-meta">
                            <time datetime="2015-06-17T20:22:05+08:00">30 minutes ago</time>
                          </div>
                          <div class="media-detail">Anyways, i would like just do it</div>
                        </div>
                      </div>
                    </a>
                    <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                      <div class="media">
                        <div class="media-left padding-right-10">
                          <span class="avatar avatar-sm avatar-off">
                            <img src="<?= base_url();?>assets/global/portraits/3.jpg" alt="..." />
                            <i></i>
                          </span>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">Caleb Richards</h6>
                          <div class="media-meta">
                            <time datetime="2015-06-17T12:30:30+08:00">12 hours ago</time>
                          </div>
                          <div class="media-detail">I checheck the document. But there seems</div>
                        </div>
                      </div>
                    </a>
                    <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                      <div class="media">
                        <div class="media-left padding-right-10">
                          <span class="avatar avatar-sm avatar-busy">
                            <img src="<?= base_url();?>assets/global/portraits/4.jpg" alt="..." />
                            <i></i>
                          </span>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">June Lane</h6>
                          <div class="media-meta">
                            <time datetime="2015-06-16T18:38:40+08:00">2 days ago</time>
                          </div>
                          <div class="media-detail">Lorem ipsum Id consectetur et minim</div>
                        </div>
                      </div>
                    </a>
                    <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                      <div class="media">
                        <div class="media-left padding-right-10">
                          <span class="avatar avatar-sm avatar-away">
                            <img src="<?= base_url();?>assets/global/portraits/5.jpg" alt="..." />
                            <i></i>
                          </span>
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">Edward Fletcher</h6>
                          <div class="media-meta">
                            <time datetime="2015-06-15T20:34:48+08:00">3 days ago</time>
                          </div>
                          <div class="media-detail">Dolor et irure cupidatat commodo nostrud nostrud.</div>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
              </li>
              <li class="dropdown-menu-footer" role="presentation">
                <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button">
                  <i class="icon md-settings" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0)" role="menuitem">
                    See all messages
                  </a>
              </li>
            </ul>
          </li>
          <li id="toggleChat">
            <a data-toggle="site-sidebar" href="javascript:void(0)" title="Chat" data-url="<?= base_url();?>assets/site-sidebar.tpl">
              <i class="icon md-comment" aria-hidden="true"></i>
            </a>
          </li>
        </ul>
        <!-- End Navbar Toolbar Right -->
      </div>
      <!-- End Navbar Collapse -->

      <!-- Site Navbar Seach -->
      <div class="collapse navbar-search-overlap" id="site-navbar-search">
        <form role="search">
          <div class="form-group">
            <div class="input-search">
              <i class="input-search-icon md-search" aria-hidden="true"></i>
              <input type="text" class="form-control" name="site-search" placeholder="Search...">
              <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
              data-toggle="collapse" aria-label="Close"></button>
            </div>
          </div>
        </form>
      </div>
      <!-- End Site Navbar Seach -->
    </div>
  </nav>
  <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu">
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>promo/manage">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Promo</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div class="site-menubar-footer">
      <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
      data-original-title="Settings">
        <span class="icon md-settings" aria-hidden="true"></span>
      </a>
      <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
        <span class="icon md-eye-off" aria-hidden="true"></span>
      </a>
      <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
        <span class="icon md-power" aria-hidden="true"></span>
      </a>
    </div>
  </div>
  <div class="site-gridmenu">
    <div>
      <div>
        <ul>
          <li>
            <a href="<?= base_url();?>assets/apps/mailbox/mailbox.html">
              <i class="icon md-email"></i>
              <span>Mailbox</span>
            </a>
          </li>
          <li>
            <a href="<?= base_url();?>assets/apps/calendar/calendar.html">
              <i class="icon md-calendar"></i>
              <span>Calendar</span>
            </a>
          </li>
          <li>
            <a href="<?= base_url();?>assets/apps/contacts/contacts.html">
              <i class="icon md-account"></i>
              <span>Contacts</span>
            </a>
          </li>
          <li>
            <a href="<?= base_url();?>assets/apps/media/overview.html">
              <i class="icon md-videocam"></i>
              <span>Media</span>
            </a>
          </li>
          <li>
            <a href="<?= base_url();?>assets/apps/documents/categories.html">
              <i class="icon md-receipt"></i>
              <span>Documents</span>
            </a>
          </li>
          <li>
            <a href="<?= base_url();?>assets/apps/projects/projects.html">
              <i class="icon md-image"></i>
              <span>Project</span>
            </a>
          </li>
          <li>
            <a href="<?= base_url();?>assets/apps/forum/forum.html">
              <i class="icon md-comments"></i>
              <span>Forum</span>
            </a>
          </li>
          <li>
            <a href="<?= base_url();?>assets/index.html">
              <i class="icon md-view-dashboard"></i>
              <span>Dashboard</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>


  <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Add Promo</h1>
      <ol class="breadcrumb">
        <li>Home</li>
        <li><a href="<?= base_url();?>promo/manage">Promo</a></li>
        <li>Add Promo</li>
      </ol>
      <div class="page-header-actions">

      </div>

    </div>
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <!-- Panel Standard Editor -->
          <div class="panel">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
              <form class="form-horizontal" action="<?= base_url();?>menu/edit" method="post" enctype="multipart/form-data">
                <div class="form-group form-material form-group form-material-lg">
                  <label class="col-sm-3 control-label" for="inputSizingLarge">Name</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="name" value="<?= $menu['name'];?>"
                    placeholder="Menu Name">

	                    <input type="text" name="id" value="<?= $menu['id'];?>" hidden
	                    placeholder="Menu Name">
                  </div>
                </div>
                <div class="form-group form-material form-group form-material-sm">
                  <label class="col-sm-3 control-label" for="inputSizingSmall">Image</label>
                  <div class="col-sm-9">
                    <input type="file" name="userfile" data-plugin="dropify" data-default-file="<?= base_url().$menu['cover'];?>"/>
                  </div>
                </div>
                <div class="form-group form-material form-group form-material-sm">
                  <label class="col-sm-3 control-label" for="inputSizingSmall">Favourite</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="favorite" id="select">
                      <option value="1" <?php if($menu['fav']==1){echo 'selected';}?>>Yes</option>
                      <option value="0" <?php if($menu['fav']==0){echo 'selected';}?>>No</option>
                    </select>
                  </div>
                </div>
                <div class="form-group form-material form-group form-material-sm">
                  <label class="col-sm-3 control-label" for="inputSizingSmall">Active</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="active" id="select">

                      <option value="1" <?php if($menu['active']==1){echo 'selected';}?>>Yes</option>
                      <option value="0" <?php if($menu['active']==0){echo 'selected';}?>>No</option>
                    </select>
                  </div>
                </div>
                <div class="form-group form-material form-group form-material-sm">
                  <label class="col-sm-3 control-label" for="inputSizingSmall">Category</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="active" id="select">
                      <?php foreach($category as $row2):?>
                      <option value="<?= $row2['id']?>" <?php if($row2['id']==$menu['idcategory']){echo 'selected';}?>><?= $row2['name']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-10">

                  </div>
                  <div class="col-md-2">
                    <button id="test" class="btn btn-alert" value="Submit"> Submit</button>
                  </div>
                </div>

              </form>


            </div>
          </div>
          <!-- End Panel Standard Editor -->

          <!-- End Panel Click To Edit -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->


  <!-- Footer -->
  <footer class="site-footer">
    <div class="site-footer-legal">© 2016 <a href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Remark</a></div>
    <div class="site-footer-right">
      Crafted with <i class="red-600 icon md-favorite"></i> by <a href="http://themeforest.net/user/amazingSurge">amazingSurge</a>
    </div>
  </footer>
  <!-- Core  -->
  <script src="<?= base_url();?>assets/global/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/animsition/animsition.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asscroll/jquery-asScroll.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/mousewheel/jquery.mousewheel.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asscrollable/jquery.asScrollable.all.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/ashoverscroll/jquery-asHoverScroll.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/waves/waves.min.js"></script>

  <!-- Plugins -->
  <script src="<?= base_url();?>assets/global/vendor/switchery/switchery.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/intro-js/intro.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/screenfull/screenfull.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/slidepanel/jquery-slidePanel.min.js"></script>

  <!-- Plugins For This Page -->

  <!-- Scripts -->
  <script src="<?= base_url();?>assets/global/js/core.min.js"></script>
  <script src="<?= base_url();?>assets/js/site.min.js"></script>

  <script src="<?= base_url();?>assets/js/sections/menu.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/menubar.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/gridmenu.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/sidebar.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/configs/config-colors.min.js"></script>
  <script src="<?= base_url();?>assets/js/configs/config-tour.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/components/asscrollable.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/animsition.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/slidepanel.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/switchery.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/tabs.min.js"></script>

    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>



    <script src="<?= base_url();?>assets/global/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?= base_url();?>assets/global/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
    <script src="<?= base_url();?>assets/examples/js/forms/uploads.min.js"></script>
    <script src="<?= base_url();?>assets/global/vendor/dropify/dropify.min.js"></script>


      <script src="<?= base_url();?>assets/global/js/components/dropify.min.js"></script>
<!--
  <script src="<?= base_url();?>assets/global/js/components/summernote.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/summernote/summernote.min.js"></script>-->


  <script src="<?= base_url();?>assets/examples/js/forms/editor-summernote.min.js"></script>



    <script>
      $(document).ready(function() {
          $('#summernote').summernote({
            height:300
          });
          $('#summernote').on('summernote.enter', function() {
            var markupStr = $('#summernote').summernote('code');
          });

          $("#test").onSubmit(function(e) {
          //  var formdata=new FormData();
            //formdata.append('cover', $('input[type=file]')[0].files[0]); //use get('files')[0]

          //  formdata.append('content',$('#summernote').summernote('code'));//you can append it to formdata with a proper parameter name
            //formdata.append('title',$('#title').summernote('code'));//you can append it to formdata with a proper parameter name
            $.ajax({
                type: "POST",
                url: "ajax/register.php",
                dataType: "text",
                enctype: 'multipart/form-data',
                data: {
                    name: $('#summernote').summernote('code'),
                    //city: $("#city").val(),
                    image: formData
                }
            });
        });

      });
    </script>
  <!-- Google Analytics -->

</body>


<!-- Mirrored from getbootstrapadmin.com/remark/material/base/forms/editor-summernote.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Nov 2017 08:37:14 GMT -->
</html>
