

	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">

			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Item Stok <small>Add</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">

						<li>
							<i class="fa fa-home"></i>
							Home
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?php base_url();?>index.php/item">Item</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							Add Item
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">

						<div class="tab-content">
							<div class="tab-pane active" id="tab_0">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>Add Data Stok
										</div>

									</div>
									<div class="portlet-body form">

										<!-- BEGIN FORM-->
										<form action="<?php base_url();?>index.php/item/addstok" method="POST" enctype="multipart/form-data" class="form-horizontal" />
											<div class="form-body">
											<p><?php echo validation_errors();?></p>
											<?php foreach($item as $row):?>
											<div class="form-group">
												<label class="col-md-3 control-label">Kode Barang</label>
												<div class="col-md-4">
													<input type="text" name="kodebarang" readonly value="<?= $row->kodebarang?>" class="form-control" placeholder="Enter text" />
													<input type="text" name="id"  value="<?= $row->id?>" hidden />

													<span class="help-block">
													</span>
												</div>
											</div>
                      <div class="form-group">
												<label class="col-md-3 control-label">Nama Barang</label>
												<div class="col-md-4">
													<input type="text" name="namabarang" readonly value="<?= $row->namabarang?>" class="form-control" placeholder="Enter text" />
													<span class="help-block">
													 </span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Stok</label>
												<div class="col-md-4">
													<input type="text" name="stok" class="form-control" placeholder="Enter text" />
													<input type="text" name="stoklama" hidden value="<?= $row->stok?>" />

													<span class="help-block">
													 </span>
												</div>
											</div>
										<?php endforeach;?>
											<div class="form-actions fluid">
												<div class="col-md-offset-3 col-md-9">
													<button type="submit" class="btn blue">Submit</button>
												</div>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
