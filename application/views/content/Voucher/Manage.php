<div class="page-content-wrapper">

  <div class="page-content">
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN STYLE CUSTOMIZER -->

    <!-- END STYLE CUSTOMIZER -->
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
         Manage Barang
        </h3>
        <ul class="page-breadcrumb breadcrumb">

          <li>
            <i class="fa fa-home"></i>
            Home
            <i class="fa fa-angle-right"></i>
          </li>
         <li><a href="<?php echo base_url();?>index.php/Makanan">
            Barang</a>
            <i class="fa fa-angle-right"></i>
          </li>

        </ul>
          <p></p>
		<p style="color:red"><?php //echo $this->session->flashdata('msg');?></p>
        <!-- END PAGE TITLE & BREADCRUMB-->
      </div>

    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-edit"></i>List Order
            </div>
          </div>

          <div class="portlet-body">
            <p style="color:red;">
              <?php if($this->session->flashdata('status')!=''){
                echo $this->session->flashdata('status');
              }?>
            </p>
            <div class="table-toolbar">
              <div class="btn-group">
                <a href="<?php echo base_url();?>index.php/order/add" class="btn green">
                Add New Order <i class="fa fa-plus"></i>
                </a>
              </div>

            </div>
          </br>
             <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
            <thead>
            <tr>
              <th>
                 Kode Order
              </th>
      			  <th>
  				       Tanggal
      			  </th>
              <th>
        				 Total Penjualan
      			  </th>
              <th>
  				       Nama Pembeli
      			  </th>
              <th>
                 Action
              </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($item as $row): ?>
            <tr>
              <td>
                <?php echo $row->kodepenjualan;?>
              </td>
		           <td>
                 <?php echo $row->tanggal;?>
              </td>
              <td>
                <?php echo $row->hargatotal;?>
              </td>
              <td>
                <?php echo $row->nama;?>
              </td>
              <td>
                <a href="<?php echo base_url();?>index.php/item/edit/<?php echo $row->id;?>">
                Invoice </a> </br>
              </td>
            </tr>
          <?php endforeach;?>
            </tbody>
            </table>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
    <!-- END PAGE CONTENT -->
  </div>
</div>
<!-- END CONTENT -->
</div>
