

  <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Promo</h1>
      <ol class="breadcrumb">
        <li>Home</li>
        <li><a href="javascript:void(0)">Promo</a></li>
      </ol>
    </div>
    <div class="page-content">
      <!-- Panel Basic -->
      <div class="panel">
        <header class="panel-heading">
          <div class="panel-actions"></div>
          <h3 class="panel-title">Manage Promo</h3>
          <div class="row" style="margin-left:15px">
            <div class="col-sm-6">
              <div class="margin-bottom-15">
                <a href="add" class="btn btn-primary" type="button">
                  <i class="icon md-plus" aria-hidden="true"></i> Add Data
                </a>
              </div>
            </div>
        </header>
        <div class="panel-body">

          <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Title</th>
                <th>Date</th>
                <th>Summary</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <!--
            <tfoot>
              <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Date</th>
                <th>Salary</th>
              </tr>
            </tfoot>-->
            <tbody>
              <?php $no=1;foreach($promo as $row):?>
              <tr>
                <td><?= $no?></td>
                <td><?php
                  if(strlen($row['title'])>=25){
                    echo substr($row['title'],0,25).'...';
                  }else{
                    echo $row['title'];
                  }?>
                </td>
                <td><?= $row['date'];?></td>
                <td><?php
                  if(strlen($row['content'])>=10){
                    echo substr($row['content'],0,10).'...';
                  }else{
                    echo $row['content'];
                  }?>
                </td>
                <td><?php
                  if($row['status']==1){
                    echo 'Publish';
                  }else{
                    echo 'Unpublished';
                  }?>
                </td>
                <td><a href="<?= base_url();?>promo/edit/<?= $row['id']?>" class="btn btn-default">Edit</a></td>
              </tr>
            <?php $no++;endforeach;?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- End Panel Basic -->
    </div>
  </div>
  <!-- End Page -->


  <!-- Footer -->
  <footer class="site-footer">
    <div class="site-footer-legal">© 2017 Naniura</div>

  </footer>
  <!-- Core  -->
  <script src="<?= base_url();?>assets/global/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/animsition/animsition.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asscroll/jquery-asScroll.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/mousewheel/jquery.mousewheel.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asscrollable/jquery.asScrollable.all.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/ashoverscroll/jquery-asHoverScroll.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/waves/waves.min.js"></script>

  <!-- Plugins -->
  <script src="<?= base_url();?>assets/global/vendor/switchery/switchery.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/intro-js/intro.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/screenfull/screenfull.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/slidepanel/jquery-slidePanel.min.js"></script>

  <!-- Plugins For This Page -->
  <script src="<?= base_url();?>assets/global/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/datatables-responsive/dataTables.responsive.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asrange/jquery-asRange.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/bootbox/bootbox.js"></script>

  <!-- Scripts -->
  <script src="<?= base_url();?>assets/global/js/core.min.js"></script>
  <script src="<?= base_url();?>assets//js/site.min.js"></script>

  <script src="<?= base_url();?>assets//js/sections/menu.min.js"></script>
  <script src="<?= base_url();?>assets//js/sections/menubar.min.js"></script>
  <script src="<?= base_url();?>assets//js/sections/gridmenu.min.js"></script>
  <script src="<?= base_url();?>assets//js/sections/sidebar.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/configs/config-colors.min.js"></script>
  <script src="<?= base_url();?>assets//js/configs/config-tour.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/components/asscrollable.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/animsition.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/slidepanel.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/switchery.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/tabs.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/components/datatables.min.js"></script>


  <script src="<?= base_url();?>assets//examples/js/tables/datatable.min.js"></script>
  <script src="<?= base_url();?>assets//examples/js/uikit/icon.min.js"></script>


</body>


<!-- Mirrored from getbootstrapadmin.com/remark/material/base/tables/datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Nov 2017 08:37:40 GMT -->
</html>
