<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<!-- Mirrored from getbootstrapadmin.com/remark/material/base/forms/editor-summernote.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Nov 2017 08:37:13 GMT -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">

  <title>Naniura Content Management System</title>

  <link rel="apple-touch-icon" href="<?= base_url();?>assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="<?= base_url();?>assets/images/favicon.ico">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/css/bootstrap.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/css/bootstrap-extend.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/css/site.min3f0d.css?v2.2.0">

  <!-- Skin tools (demo site only) -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/css/skintools.min3f0d.css?v2.2.0">
  <script src="<?= base_url();?>assets/js/sections/skintools.min.js"></script>

  <!-- Plugins -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/animsition/animsition.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/asscrollable/asScrollable.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/switchery/switchery.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/intro-js/introjs.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/slidepanel/slidePanel.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/flag-icon-css/flag-icon.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/waves/waves.min3f0d.css?v2.2.0">

  <!-- Plugins For This Page -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/blueimp-file-upload/jquery.fileupload.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/dropify/dropify.min3f0d.css?v2.2.0">
      <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">

  <!-- Fonts -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/fonts/material-design/material-design.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/fonts/brand-icons/brand-icons.min3f0d.css?v2.2.0">

  <link rel='stylesheet' href='<?= base_url();?>assets/<?= base_url();?>assets/<?= base_url();?>assets/fonts.googleapis.com/css5478.css?family=Roboto:400,400italic,700'>

  <link rel="stylesheet" href="<?= base_url();?>assets/global/fonts/font-awesome/font-awesome.min3f0d.css?v2.2.0">

  <!--[if lt IE 9]>
    <script src="<?= base_url();?>assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->

  <!--[if lt IE 10]>
    <script src="<?= base_url();?>assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="<?= base_url();?>assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->

  <!-- Scripts -->
  <script src="<?= base_url();?>assets/global/vendor/modernizr/modernizr.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/breakpoints/breakpoints.min.js"></script>
  <script>
    Breakpoints();
  </script>
  <style>
    [21:17, 1/29/2018] Angga Okz: .loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}

/* Transparent Overlay */
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>

</head>
<body>
    <div class="loading"></div>
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

  <nav class="site-navbar navbar navbar-inverse navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
      data-toggle="menubar">
        <span class="sr-only">Toggle navigation</span>
        <span class="hamburger-bar"></span>
      </button>
      <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
      data-toggle="collapse">
        <i class="icon md-more" aria-hidden="true"></i>
      </button>
      <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
        <!--<img class="navbar-brand-logo" src="<?= base_url();?>assets//images/logo.png" title="Remark">-->
        <span class="navbar-brand-text hidden-xs"> Naniura</span>
      </div>
      <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-search"
      data-toggle="collapse">
        <span class="sr-only">Toggle Search</span>
        <i class="icon md-search" aria-hidden="true"></i>
      </button>
    </div>

    <div class="navbar-container container-fluid">
      <!-- Navbar Collapse -->
     
      <!-- End Navbar Collapse -->

      <!-- Site Navbar Seach -->
      
      <!-- End Site Navbar Seach -->
    </div>
  </nav>
  <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu">
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>promo/manage">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Promo & Event</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>menu/manage">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Menu</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>voucher/manage">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Voucher</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>member/manage">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Member</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>slider">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Slider</span>
              </a>
            </li>
          </div>
        </div>
      </div>
    </div>

    <div class="site-menubar-footer">
    
      <a href="<?= base_url();?>member/logout" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
        <span class="icon md-power" aria-hidden="true"></span>
      </a>
    </div>
  </div>

  <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Add Promo</h1>
      <ol class="breadcrumb">
        <li>Home</li>
        <li><a href="<?= base_url();?>promo/manage">Promo</a></li>
        <li>Edit Promo</li>
      </ol>
      <div class="page-header-actions">

      </div>

    </div>
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <!-- Panel Standard Editor -->
          <div class="panel">
            <div class="panel-heading">
            </div>

            <div class="panel-body">
                <div class="form-group form-material form-group form-material-lg">
                  <label class="col-sm-3 control-label" for="inputSizingLarge">Title</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="title" value="<?= $promo[0]['title']?>"
                    placeholder="Insert Title Here..">
                      <input type="text" id="id" hidden value="<?= $promo[0]['id']?>"
                      >
                  </div>
                </div>
                <div class="form-group form-material form-group form-material-sm">
                  <label class="col-sm-3 control-label" for="inputSizingSmall">Content</label>
                  <div class="col-sm-9">
                    <textarea class="input-block-level" id="summernote" name="content" rows="18"><?= $promo[0]['content']?>
</textarea>
                  </div>
                </div>
                <div class="form-group form-material form-group form-material-sm">
                  <label class="col-sm-3 control-label" for="inputSizingSmall">Cover Image</label>
                  <div class="col-sm-9">
                    <input type="file" id="input-file" data-plugin="dropify" data-default-file="<?= $promo[0]['cover']?> "/>
                  </div>
                </div>

                <div class="form-group form-material form-group form-material-sm">
                  <label class="col-sm-3 control-label" for="inputSizingSmall">Active</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="status" id="select">
                      <option value="1" <?php if($promo[0]['status']==1){echo 'selected';}?>>Yes</option>
                      <option value="0" <?php if($promo[0]['status']==0){echo 'selected';}?>>No</option>
                    </select>
                  </div>
                </div>


                <div class="row">
                  <div class="col-md-10">

                  </div>
                  <div class="col-md-2">
                    <button id="test" class="btn btn-alert" value="Submit"> Submit</button>
                  </div>
                </div>


            </div>
          </div>
          <!-- End Panel Standard Editor -->

          <!-- End Panel Click To Edit -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->


  <!-- Footer -->
  <footer class="site-footer">
    <div class="site-footer-legal">© 2017 Naniura
    </div>
  </footer>
  <!-- Core  -->
  <script src="<?= base_url();?>assets/global/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/animsition/animsition.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asscroll/jquery-asScroll.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/mousewheel/jquery.mousewheel.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asscrollable/jquery.asScrollable.all.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/ashoverscroll/jquery-asHoverScroll.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/waves/waves.min.js"></script>

  <!-- Plugins -->
  <script src="<?= base_url();?>assets/global/vendor/switchery/switchery.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/intro-js/intro.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/screenfull/screenfull.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/slidepanel/jquery-slidePanel.min.js"></script>

  <!-- Plugins For This Page -->

  <!-- Scripts -->
  <script src="<?= base_url();?>assets/global/js/core.min.js"></script>
  <script src="<?= base_url();?>assets/js/site.min.js"></script>

  <script src="<?= base_url();?>assets/js/sections/menu.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/menubar.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/gridmenu.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/sidebar.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/configs/config-colors.min.js"></script>
  <script src="<?= base_url();?>assets/js/configs/config-tour.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/components/asscrollable.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/animsition.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/slidepanel.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/switchery.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/tabs.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>



    <script src="<?= base_url();?>assets/global/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?= base_url();?>assets/global/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
    <script src="<?= base_url();?>assets/examples/js/forms/uploads.min.js"></script>
    <script src="<?= base_url();?>assets/global/vendor/dropify/dropify.min.js"></script>


      <script src="<?= base_url();?>assets/global/js/components/dropify.min.js"></script>
<!--
  <script src="<?= base_url();?>assets/global/js/components/summernote.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/summernote/summernote.min.js"></script>-->


  <script src="<?= base_url();?>assets/examples/js/forms/editor-summernote.min.js"></script>


    <script>
      $(document).ready(function() {
          $(document).ready(function() {
	$('#summernote').summernote({
		height: "500px"
	});
});
var postForm = function() {
	var content = $('textarea[name="content"]').html($('#summernote').code());
}


          $("#test").click(function() {
          //  var formdata=new FormData();
            //formdata.append('cover', $('input[type=file]')[0].files[0]); //use get('files')[0]

          //  formdata.append('content',$('#summernote').summernote('code'));//you can append it to formdata with a proper parameter name
            //formdata.append('title',$('#title').summernote('code'));//you can append it to formdata with a proper parameter name

            var form_data = new FormData();
            var file_data = $('#input-file').prop('files')[0];
            console.log($('#input-file')[0].files[0]);
            form_data.append('cover',file_data);

            form_data.append('content',$('#summernote').summernote('code'));
            form_data.append('title',$("#title").val());
            form_data.append('id',$("#id").val());
            form_data.append('status',$("#select").val());
            $.ajax({
                url: "<?= base_url();?>index.php/promo/edit",
                type: "POST",
                data:  form_data,
                contentType: false,
                mimeType: "multipart/form-data",
                cache: false,
                processData:false,
                success: function(data){
                    
                    window.location.replace("http://naniurasushi.com/promo/manage");
                },
                error: function (textStatus, errorThrown) {
            }

            });
        });

      });
    </script>
  <!-- Google Analytics -->

</body>


<!-- Mirrored from getbootstrapadmin.com/remark/material/base/forms/editor-summernote.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Nov 2017 08:37:14 GMT -->
</html>
