<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<!-- Mirrored from getbootstrapadmin.com/remark/material/base/forms/editor-summernote.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Nov 2017 08:37:13 GMT -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">

  <title>Summernote | Remark Material Admin Template</title>

  <link rel="apple-touch-icon" href="<?= base_url();?>assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="<?= base_url();?>assets/images/favicon.ico">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/css/bootstrap.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/css/bootstrap-extend.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/css/site.min3f0d.css?v2.2.0">

  <!-- Skin tools (demo site only) -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/css/skintools.min3f0d.css?v2.2.0">
  <script src="<?= base_url();?>assets/js/sections/skintools.min.js"></script>

  <!-- Plugins -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/animsition/animsition.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/asscrollable/asScrollable.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/switchery/switchery.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/intro-js/introjs.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/slidepanel/slidePanel.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/flag-icon-css/flag-icon.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/waves/waves.min3f0d.css?v2.2.0">

  <!-- Plugins For This Page -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/blueimp-file-upload/jquery.fileupload.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/dropify/dropify.min3f0d.css?v2.2.0">
      <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">

  <!-- Fonts -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/fonts/material-design/material-design.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/fonts/brand-icons/brand-icons.min3f0d.css?v2.2.0">

  <link rel='stylesheet' href='<?= base_url();?>assets/<?= base_url();?>assets/<?= base_url();?>assets/fonts.googleapis.com/css5478.css?family=Roboto:400,400italic,700'>

  <link rel="stylesheet" href="<?= base_url();?>assets/global/fonts/font-awesome/font-awesome.min3f0d.css?v2.2.0">

  <!--[if lt IE 9]>
    <script src="<?= base_url();?>assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->

  <!--[if lt IE 10]>
    <script src="<?= base_url();?>assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="<?= base_url();?>assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->

  <!-- Scripts -->
  <script src="<?= base_url();?>assets/global/vendor/modernizr/modernizr.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/breakpoints/breakpoints.min.js"></script>
  <script>
    Breakpoints();
  </script>
</head>
<body>
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

  <nav class="site-navbar navbar navbar-inverse navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
      data-toggle="menubar">
        <span class="sr-only">Toggle navigation</span>
        <span class="hamburger-bar"></span>
      </button>
      <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
      data-toggle="collapse">
        <i class="icon md-more" aria-hidden="true"></i>
      </button>
      <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" >
        <!--<img class="navbar-brand-logo" src="<?= base_url();?>assets//images/logo.png" title="Remark">-->
        <span class="navbar-brand-text hidden-xs"> Naniura</span>
      </div>
      <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-search"
      data-toggle="collapse">
        <span class="sr-only">Toggle Search</span>
        <i class="icon md-search" aria-hidden="true"></i>
      </button>
    </div>

    <div class="navbar-container container-fluid">
      <!-- Navbar Collapse -->
      <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
        <!-- Navbar Toolbar -->
        <ul class="nav navbar-toolbar">
          
        </ul>
        <!-- End Navbar Toolbar -->

        <!-- Navbar Toolbar Right -->
        <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
          
        </ul>
        <!-- End Navbar Toolbar Right -->
      </div>
      <!-- End Navbar Collapse -->

      <!-- Site Navbar Seach -->
      <div class="collapse navbar-search-overlap" id="site-navbar-search">
        <form role="search">
          <div class="form-group">
            <div class="input-search">
              <i class="input-search-icon md-search" aria-hidden="true"></i>
              <input type="text" class="form-control" name="site-search" placeholder="Search...">
              <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
              data-toggle="collapse" aria-label="Close"></button>
            </div>
          </div>
        </form>
      </div>
      <!-- End Site Navbar Seach -->
    </div>
  </nav>
  <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu">
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>promo/manage">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Promo & Event</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>menu/manage">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Menu</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>voucher/manage">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Voucher</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>member/manage">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Member</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>slider">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Slider</span>
              </a>
            </li>
          </div>
        </div>
      </div>
    </div>

    <div class="site-menubar-footer">
    
      <a href="<?= base_url();?>member/logout" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
        <span class="icon md-power" aria-hidden="true"></span>
      </a>
    </div>
  </div>
  <div class="site-gridmenu">
    <div>
      <div>
        <ul>
          
        </ul>
      </div>
    </div>
  </div>
  <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Edit Menu</h1>
      <ol class="breadcrumb">
        <li>Home</li>
        <li><a href="<?= base_url();?>menu/manage">Menu</a></li>
        <li>Edit Menu</li>
      </ol>
      <div class="page-header-actions">

      </div>

    </div>
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <!-- Panel Standard Editor -->
          <div class="panel">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
              <form class="form-horizontal" action="<?= base_url();?>menu/edit" method="post" enctype="multipart/form-data">
                <div class="form-group form-material form-group form-material-lg">
                  <label class="col-sm-3 control-label" for="inputSizingLarge">Name</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="name" value="<?= $menu['name'];?>"
                    placeholder="Menu Name">

	                    <input type="text" name="id" value="<?= $menu['id'];?>" hidden
	                    placeholder="Menu Name">
                  </div>
                </div>
                <div class="form-group form-material form-group form-material-sm">
                  <label class="col-sm-3 control-label" for="inputSizingSmall">Image</label>
                  <div class="col-sm-9">
                    <input type="file" name="userfile" data-plugin="dropify" data-default-file="<?= base_url().$menu['cover'];?>"/>
                  </div>
                </div>
                <div class="form-group form-material form-group form-material-sm">
                  <label class="col-sm-3 control-label" for="inputSizingSmall">Favourite</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="favorite" id="select">
                      <option value="1" <?php if($menu['fav']==1){echo 'selected';}?>>Yes</option>
                      <option value="0" <?php if($menu['fav']==0){echo 'selected';}?>>No</option>
                    </select>
                  </div>
                </div>
                <div class="form-group form-material form-group form-material-sm">
                  <label class="col-sm-3 control-label" for="inputSizingSmall">Active</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="active" id="select">

                      <option value="1" <?php if($menu['active']==1){echo 'selected';}?>>Yes</option>
                      <option value="0" <?php if($menu['active']==0){echo 'selected';}?>>No</option>
                    </select>
                  </div>
                </div>
                <div class="form-group form-material form-group form-material-sm">
                  <label class="col-sm-3 control-label" for="inputSizingSmall">Category</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="active" id="select">
                      <?php foreach($category as $row2):?>
                      <option value="<?= $row2['id']?>" <?php if($row2['id']==$menu['idcategory']){echo 'selected';}?>><?= $row2['name']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-10">

                  </div>
                  <div class="col-md-2">
                    <button id="test" class="btn btn-alert" value="Submit"> Submit</button>
                  </div>
                </div>

              </form>


            </div>
          </div>
          <!-- End Panel Standard Editor -->

          <!-- End Panel Click To Edit -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->


  <!-- Footer -->
  <footer class="site-footer">
    <div class="site-footer-legal">© 2016 <a href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Remark</a></div>
    <div class="site-footer-right">
      Crafted with <i class="red-600 icon md-favorite"></i> by <a href="http://themeforest.net/user/amazingSurge">amazingSurge</a>
    </div>
  </footer>
  <!-- Core  -->
  <script src="<?= base_url();?>assets/global/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/animsition/animsition.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asscroll/jquery-asScroll.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/mousewheel/jquery.mousewheel.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asscrollable/jquery.asScrollable.all.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/ashoverscroll/jquery-asHoverScroll.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/waves/waves.min.js"></script>

  <!-- Plugins -->
  <script src="<?= base_url();?>assets/global/vendor/switchery/switchery.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/intro-js/intro.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/screenfull/screenfull.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/slidepanel/jquery-slidePanel.min.js"></script>

  <!-- Plugins For This Page -->

  <!-- Scripts -->
  <script src="<?= base_url();?>assets/global/js/core.min.js"></script>
  <script src="<?= base_url();?>assets/js/site.min.js"></script>

  <script src="<?= base_url();?>assets/js/sections/menu.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/menubar.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/gridmenu.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/sidebar.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/configs/config-colors.min.js"></script>
  <script src="<?= base_url();?>assets/js/configs/config-tour.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/components/asscrollable.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/animsition.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/slidepanel.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/switchery.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/tabs.min.js"></script>

    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>



    <script src="<?= base_url();?>assets/global/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?= base_url();?>assets/global/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
    <script src="<?= base_url();?>assets/examples/js/forms/uploads.min.js"></script>
    <script src="<?= base_url();?>assets/global/vendor/dropify/dropify.min.js"></script>


      <script src="<?= base_url();?>assets/global/js/components/dropify.min.js"></script>
<!--
  <script src="<?= base_url();?>assets/global/js/components/summernote.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/summernote/summernote.min.js"></script>-->


  <script src="<?= base_url();?>assets/examples/js/forms/editor-summernote.min.js"></script>



    <script>
      $(document).ready(function() {
          $('#summernote').summernote({
            height:300
          });
          $('#summernote').on('summernote.enter', function() {
            var markupStr = $('#summernote').summernote('code');
          });

          $("#test").onSubmit(function(e) {
          //  var formdata=new FormData();
            //formdata.append('cover', $('input[type=file]')[0].files[0]); //use get('files')[0]

          //  formdata.append('content',$('#summernote').summernote('code'));//you can append it to formdata with a proper parameter name
            //formdata.append('title',$('#title').summernote('code'));//you can append it to formdata with a proper parameter name
            $.ajax({
                type: "POST",
                url: "ajax/register.php",
                dataType: "text",
                enctype: 'multipart/form-data',
                data: {
                    name: $('#summernote').summernote('code'),
                    //city: $("#city").val(),
                    image: formData
                }
            });
        });

      });
    </script>
  <!-- Google Analytics -->

</body>


<!-- Mirrored from getbootstrapadmin.com/remark/material/base/forms/editor-summernote.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Nov 2017 08:37:14 GMT -->
</html>
