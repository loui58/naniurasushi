
  <!-- Page -->
  <div class="page animsition">
    <div class="page-header">
      <h1 class="page-title">Manage Slider</h1>
      <ol class="breadcrumb">
        <li>Home</li>
        <li>Slider</li>
      </ol>
      <div class="page-header-actions">

      </div>

    </div>
    <div class="page-content container-fluid">

      <div class="row">
          <?php if(count($slider)<10):?>
          <div class="col-xs-12" style="margin-bottom:30px">
               <a href="<?= base_url();?>slider/add" class="btn btn-success" > Add Slider</a>
          </div>
          <?php endif;?>
        <div class="col-xs-12">
          <!-- Panel Standard Editor -->
          <?php $no=1;foreach($slider as $s):?>
          <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">Slider <?= $no?></h3>
            </div>
            <div class="panel-body">
              <form class="form-horizontal" action="<?= base_url();?>slider/update/<?= $s['id'];?>" method="post" enctype="multipart/form-data">

                <div class="form-group form-material form-group form-material-sm">
                  <label class="col-sm-3 control-label" for="inputSizingSmall">Image</label>
                  <div class="col-sm-9">
                    <input type="file" name="userfile" data-plugin="dropify" data-default-file="<?= $s['cover'];?>"/>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8">
                      
                  </div>
                  <div class="col-md-2">
                      <a href="<?= base_url();?>slider/delete/<?= $s['id']?>" class="btn btn-danger pull-right" > Delete Slider</a>
                  </div>
                  <div class="col-md-2">
                    <button id="test" class="btn btn-alert" value="Submit"> Submit</button>
                  </div>
                </div>

              </form>


            </div>
          </div>
          <?php $no++;endforeach;?>
          <!-- End Panel Standard Editor -->

          <!-- End Panel Click To Edit -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->


  <!-- Footer -->
  <footer class="site-footer">
    <div class="site-footer-legal">© 2016 <a href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Remark</a></div>
    <div class="site-footer-right">
      Crafted with <i class="red-600 icon md-favorite"></i> by <a href="http://themeforest.net/user/amazingSurge">amazingSurge</a>
    </div>
  </footer>
  <!-- Core  -->
  <script src="<?= base_url();?>assets/global/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/animsition/animsition.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asscroll/jquery-asScroll.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/mousewheel/jquery.mousewheel.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asscrollable/jquery.asScrollable.all.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/ashoverscroll/jquery-asHoverScroll.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/waves/waves.min.js"></script>

  <!-- Plugins -->
  <script src="<?= base_url();?>assets/global/vendor/switchery/switchery.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/intro-js/intro.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/screenfull/screenfull.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/slidepanel/jquery-slidePanel.min.js"></script>

  <!-- Plugins For This Page -->

  <!-- Scripts -->
  <script src="<?= base_url();?>assets/global/js/core.min.js"></script>
  <script src="<?= base_url();?>assets/js/site.min.js"></script>

  <script src="<?= base_url();?>assets/js/sections/menu.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/menubar.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/gridmenu.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/sidebar.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/configs/config-colors.min.js"></script>
  <script src="<?= base_url();?>assets/js/configs/config-tour.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/components/asscrollable.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/animsition.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/slidepanel.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/switchery.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/tabs.min.js"></script>

    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>



    <script src="<?= base_url();?>assets/global/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?= base_url();?>assets/global/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
    <script src="<?= base_url();?>assets/examples/js/forms/uploads.min.js"></script>
    <script src="<?= base_url();?>assets/global/vendor/dropify/dropify.min.js"></script>


      <script src="<?= base_url();?>assets/global/js/components/dropify.min.js"></script>
<!--
  <script src="<?= base_url();?>assets/global/js/components/summernote.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/summernote/summernote.min.js"></script>-->


  <script src="<?= base_url();?>assets/examples/js/forms/editor-summernote.min.js"></script>



    <script>
      $(document).ready(function() {
          $('#summernote').summernote({
            height:300
          });
          $('#summernote').on('summernote.enter', function() {
            var markupStr = $('#summernote').summernote('code');
          });

          $("#test").onSubmit(function(e) {
          //  var formdata=new FormData();
            //formdata.append('cover', $('input[type=file]')[0].files[0]); //use get('files')[0]

          //  formdata.append('content',$('#summernote').summernote('code'));//you can append it to formdata with a proper parameter name
            //formdata.append('title',$('#title').summernote('code'));//you can append it to formdata with a proper parameter name
            $.ajax({
                type: "POST",
                url: "ajax/register.php",
                dataType: "text",
                enctype: 'multipart/form-data',
                data: {
                    name: $('#summernote').summernote('code'),
                    //city: $("#city").val(),
                    image: formData
                }
            });
        });

      });
    </script>
  <!-- Google Analytics -->

</body>


<!-- Mirrored from getbootstrapadmin.com/remark/material/base/forms/editor-summernote.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Nov 2017 08:37:14 GMT -->
</html>
