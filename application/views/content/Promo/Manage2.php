<div class="page animsition">
<div class="page-header">
  <h1 class="page-title">Manage Barang</h1>
  <ol class="breadcrumb">
    <li><a href="../index.html">Home</a></li>
    <li class="active">Barang</li>
  </ol>

</div>
<div class="page-content">
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title">List Barang</h3>
  </header>
  <div class="panel-body">
    <div class="row">
      <div class="col-sm-6">
        <div class="margin-bottom-15">
          <!--<button id="addToTable" class="btn btn-primary" type="button">
            <i class="icon md-plus" aria-hidden="true"></i> Tambah Barang
          </button>-->
        </div>
      </div>
    </div>
    <table class="table table-bordered table-hover table-striped" id="exampleAddRow">
      <thead>
        <tr>
          <th>
             Kode Barang
          </th>
          <th>
             Nama Barang
          </th>
          <th>
             Harga Jual
          </th>
          <!--<th>
             Harga Beli
          </th>
          <th>
              Stok
          </th>
          <th>
             Action
          </th>-->
        </tr>
      </thead>
      <tbody>
        <?php //foreach($item as $row): ?>
        <tr class="gradeA">
          <td>
            <?php //echo $row->kodebarang;?>
          </td>
           <td>
             <?php //echo $row->namabarang;?>
          </td>
          <td>
            <?php //echo "Rp " . number_format($row->hargajual,2,',','.');?>
          </td>
          <!--<td>
            <?php //echo "Rp " . number_format($row->hargabeli,2,',','.');?>
          </td>
          <td>
            <?php //echo $row->stok;?>
          </td>
          <td>

            <a href="<?php echo base_url();?>index.php/item/edit/<?php echo $row->id;?>" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
            data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a>
            <a href="<?php echo base_url();?>index.php/item/addstok/<?php echo $row->id;?>">
            Tambah Stok </a>
          </td>-->
        </tr>
      <?php //endforeach;?>

      </tbody>
    </table>
  </div>
</div>

    </div>
  </div>
