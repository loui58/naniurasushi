
        <div class="page-content-wrapper ">

            <div class="content ">

                <div class=" container-fluid   container-fixed-lg">

                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Form layouts</li>
                    </ol>

                    <div class="row">
                        <div class="col-xl-7 col-lg-6 ">
                          <?php foreach($item as $row):?>

                            <div class="card card-transparent">
                                <div class="card-block">
                                    <form id="form-personal" role="form" autocomplete="off">
                                        <div class="row clearfix">
                                            <div class="col-md-6">
                                                <div class="form-group form-group-default required">
                                                    <label>Kode Produk</label>
                                                    <input type="text" name="kodebarang" readonly value="<?= $row->kodebarang?>" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-group-default">
                                                    <label>Nama</label>
                                                    <input type="text" class="form-control" name="name" required>
                                                    <input type="text" name="id"  value="<?= $row->id?>" hidden />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-default input-group">
                                                    <div class="form-input-group">
                                                        <label>Stok Masuk</label>
                                                        <input type="text" class="form-control" name="stok" placeholder="yourname.pages.com (this can be changed later)" required>
                                                    </div>
                                                    <div class="input-group-addon d-flex ">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <button class="btn btn-primary" type="submit">Create a new account</button>
                                    </form>
                                </div>
                            </div>
                          <?php endforeach;?>
                        </div>
                        <div class="col-xl-5 col-lg-6">

                            <div class="card card-transparent">
                                <div class="card-header ">
                                    <div class="card-title">Minyak Kutus Kutus
                                    </div>
                                </div>
                                <div class="card-block">
                                    <h3>Tamba Waras</h3>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
