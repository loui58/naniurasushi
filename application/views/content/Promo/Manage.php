<div class="page-content-wrapper">

  <div class="page-content">
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN STYLE CUSTOMIZER -->

    <!-- END STYLE CUSTOMIZER -->
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
         Manage Barang
        </h3>
        <ul class="page-breadcrumb breadcrumb">

          <li>
            <i class="fa fa-home"></i>
            Home
            <i class="fa fa-angle-right"></i>
          </li>
         <li><a href="<?php echo base_url();?>index.php/Makanan">
            Barang</a>
            <i class="fa fa-angle-right"></i>
          </li>

        </ul>
          <p></p>
		<p style="color:red"><?php //echo $this->session->flashdata('msg');?></p>
        <!-- END PAGE TITLE & BREADCRUMB-->
      </div>

    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-edit"></i>List Barang
            </div>
          </div>

          <div class="portlet-body">
            <p style="color:red;">
              <?php if($this->session->flashdata('status')!=''){
                echo $this->session->flashdata('status');
              }?>
            </p>
            <div class="table-toolbar">
              <div class="btn-group">
                <a href="<?php echo base_url();?>index.php/item/add" class="btn green">
                Add New <i class="fa fa-plus"></i>
                </a>
              </div>

            </div>
          </br>
             <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
            <thead>
            <tr>
              <th>
                 Kode Barang
              </th>
      			  <th>
  				       Nama Barang
      			  </th>
              <th>
        				 Harga Jual
      			  </th>
              <th>
  				       Harga Beli
      			  </th>
              <th>
  				        Stok
      			  </th>
              <th>
                 Action
              </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($item as $row): ?>
            <tr>
              <td>
                <?php echo $row->kodebarang;?>
              </td>
		           <td>
                 <?php echo $row->namabarang;?>
              </td>
              <td>
                <?php echo $row->hargajual;?>
              </td>
              <td>
                <?php echo $row->hargabeli;?>
              </td>
              <td>
                <?php echo $row->stok;?>
              </td>
              <td>
                <a href="<?php echo base_url();?>index.php/item/edit/<?php echo $row->id;?>">
                Edit </a> </br>
                <a href="<?php echo base_url();?>index.php/item/addstok/<?php echo $row->id;?>">
                Tambah Stok </a>
              </td>
            </tr>
          <?php endforeach;?>
            </tbody>
            </table>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
    <!-- END PAGE CONTENT -->
  </div>
</div>
<!-- END CONTENT -->
</div>
