<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Naniura Sushi</title>
<meta name="Keywords" content="naniura,sushi,jakarta,sashimi,ramen,iobitdev,io,bit,dev,iobit">
<meta name="Description" content="Naniura in Batak Language is a Fish That is Cooked Without Cooking, This Fish is not Fried or Baked. Only the spices and the Sour of Jungga Acids (Citrus Jambhiri) are used to Ripen The Fish. The Fish is Marinated into The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten. The Seasoning That Can Chemically Transform Raw Fish into Cook FIsh Which does not Taste Fishy and be Ready to be eaten.">
<base href="<?php print base_url(); ?>" />

<meta name="copyright" content="Naniura Sushi and naniurasushi.com 2017 All Rights Reserved Powered and Developed by IOBITDEV">
<meta name="author" content="IOBITDEV">
<meta name="robots" content="index,follow" />
<meta property="og:type" content="website" />
<meta property="og:image" content="<?= base_url();?>assets/img/img-share.jpg" />

<link rel="icon" href="<?= base_url();?>assets/img/favicon.png">

<script src="<?= base_url();?>assets/js/jquery-latest.js"></script>
<script src="<?= base_url();?>assets/js/jquery.modal.min.js"></script>
<link rel="stylesheet" href="<?= base_url();?>assets/css/jquery.modal.min.css" />

<link href="<?= base_url();?>assets/css/swiper.min.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/naniura.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/component.css" rel="stylesheet" type="text/css" >


<script type="text/javascript">
	$( document ).ready(function() {

		$('#subscribe').on('submit', function(e) {
		              e.preventDefault();
		    $.ajax({
                url:"<?= base_url();?>member/subscribe",
                method:"POST", 
                data:{
                  email: $('#email').val(), 
                },
                success:function(response) {
                 $("#subscribe-modal").modal({
    			  fadeDuration: 1000,
    			  fadeDelay: 0.50
    			});
    		   return false;
               },
               error:function(){
               }
        
            });
		    
		});
	});
</script>

</head>

<body>
<div class="wrapper">

<div class="reg-pop">
	<div class="grab-text">grab some free gelato <span class="textbold">for naniura members</span></div>
	<div class="button-pop row-reg">
		<a href="member/register" target="_blank">
			<div class="pop-reg col-reg">register here</div>
		</a>
		<a href="member">
			<div class="pop-already col-reg">im already member :)</div>
		</a>
	</div>
	<div class="button-regpop"><img src="<?= base_url();?>assets/img/cross.png" class="cross-pop" width="30"></div>
</div>

<div id="cover-regpop"></div>

	<input type="checkbox" id="op"></input>
	<div class="lower">
	  <label for="op"><img src="<?= base_url();?>assets/img/menu-m.svg" width="36"></label>
	</div>

<div class="overlay overlay-hugeinc">
			<label for="op"></label>
			<nav class="navburger">
				<ul>
					<li class="navburger-logo"><a href="<?= base_url();?>"><img src="<?= base_url();?>assets/img/brandlogo-m-black.svg" width="107"></a></li>
					<li><a href="member">Member</a></li>
					<div class="menumobile-line"></div>
					<li><a href="menu">Menu</a></li>
					<div class="menumobile-line"></div>
					<li><a href="promo">Promo &amp; Event</a></li>
					<div class="menumobile-line"></div>
					<li><a href="bonbon"><span class="text-bonbon">Bon Bon Gelato</span></a></li>
					<div class="menumobile-line"></div>
					<li><a href="shop">Shop</a></li>
					<div class="menumobile-line"></div>
					<li><a href="location">Location &amp; Contact</a></li>
					<div class="menumobile-line"></div>
				</ul>
			</nav>

<div class="overlay-black"></div>
</div>

	<nav id="menu-fixed" class="topmenu-fix">
		<div class="mf-nav">
			<a href="<?= base_url();?>"><img src="<?= base_url();?>assets/img/brandlogo-white.svg" alt="Naniura Sushi" width="145" class="floatl"></a>
				<ul class="floatr">
					<li><a href="member">Member<span class="mf-badge"></span></a></li>
					<li><a href="menu">Menu</a></li>
					<li><a href="promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="bonbon"><img src="<?= base_url();?>assets/img/bonbon-white.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="shop">Shop</a></li>
					<li><a href="location">Location &amp; Contact</a></li>
				</ul>
		</div>
		<div class="nav-m-fixed">
			<div class="brandlogo-m"><a href=""><img src="<?= base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
	</nav>
