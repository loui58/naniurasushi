<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Naniura Sushi // Profile</title>
<meta name="Keywords" content="naniura,sushi,jakarta,sashimi,ramen,iobitdev,io,bit,dev,iobit">
<meta name="Description" content="Naniura in Batak Language is a Fish That is Cooked Without Cooking, This Fish is not Fried or Baked. Only the spices and the Sour of Jungga Acids (Citrus Jambhiri) are used to Ripen The Fish. The Fish is Marinated into The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten. The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten.">

<meta name="copyright" content="Naniura Sushi and naniurasushi.com 2017 All Rights Reserved Powered and Developed by IOBITDEV">
<meta name="author" content="IOBITDEV">
<meta name="robots" content="index,follow" />
<meta property="og:type" content="website" />
<meta property="og:image" content="<?=base_url();?>assets/img/img-share.jpg" />

<link rel="icon" href="<?=base_url();?>assets/img/favicon.png">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="<?= base_url();?>assets/css/popup.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/swiper.min.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/naniura.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/component.css" rel="stylesheet" type="text/css" >
<script src="<?= base_url();?>assets/js/jquery-latest.js"></script>

</head>

<body class="wrapper-member">
<div class="wrapper">

<!-- <div class="reg-pop">
	<div class="grab-text">grab some free gelato <span class="textbold">for naniura members</span></div>
	<div class="button-pop row-reg">
		<a href="register.html" target="_blank">
			<div class="pop-reg col-reg">register here</div>
		</a>
			<div class="pop-already col-reg">im already member :)</div>
	</div>
	<div class="button-regpop"><img src="img/cross.png" class="cross-pop" width="30"></div>
</div> -->

<!-- <div id="cover-regpop"></div> -->

<input type="checkbox" id="op"></input>
	<div class="lower">
	  <label for="op"><img src="<?=base_url();?>assets/img/menu-m.svg" width="36"></label>
	</div>

<div class="overlay overlay-hugeinc">
			<label for="op"></label>
			<nav class="navburger">
				<ul>
					<li class="navburger-logo"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m-black.svg" width="107"></a></li>
					<li><a href="<?= base_url();?>member">Member</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>bonbon"><span class="text-bonbon">Bon Bon Gelato</span></a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					<div class="menumobile-line"></div>
				</ul>
			</nav>

<div class="overlay-black"></div>
</div>

	<nav id="menu-fixed" class="topmenu-fix">
		<div class="mf-nav">
			<a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-white.svg" alt="Naniura Sushi" width="145" class="floatl"></a>
				<ul class="floatr">
					<li class="li-active"><a href="<?= base_url();?>member">Member<span class="mf-badge"></span></a></li>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?=base_url();?>assets/img/bonbon-white.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
				</ul>
		</div>
		<div class="nav-m-fixed">
			<div class="brandlogo-m"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
	</nav>

	<header>
		<div class="header-member"></div>
		<div class="wrapper-nav-m">
			<div class="brandlogo-m"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
			<div class="header-overlay">
				<a href="<?= base_url();?>"><img src="<?= base_url();?>assets/img/brandlogo.svg" alt="Naniura Sushi" width="150" class="brandlogo"></a>
				<nav class="topmenu cl-effect-4">
					<ul>
					<li class="cl-active"><a href="<?= base_url();?>member">Member<span class="nav-badge"></span></a></li>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?=base_url();?>assets/img/bonbon-logo.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					</ul>
				</nav>
				<!-- <div class="welcome-text">Best Place To Eat Sushi In<br>Our Neighbourhood</div>
				<a href="#"><button class="see-our-menu">SEE OUR MENU</button></a> -->
<!-- 				<div class="welcome-scroll">
					<a href="#title-menu">
				<div class="arrow-animate"><a href="#scrollmenu"><span></span></a></div>
				</a></div> -->
			</div>
		</header>
<section class="member-page">
<div class="redeem-wrp">
	<div class="member-line5"></div>
	<div class="redeem-title">Redeem Your Point Now<br>And Get All of This</div>
<?php foreach($voucher as $row):?>
	<div class="redeem-point-wrp" style="background-image:url('<?= base_url().$row['cover'];?>')">
		<div class="redeem-overlay"></div>
			<div class="redeem-detail-wrp">
				<div class="redeem-detail-1"><?= $row['title']?></div>
				<div class="redeem-detail-2">(<?= $row['point']?>)</div>
				<button class="redeem-btn"  id="redeem-btn<?= $row['id'];?>">redeem</button>
			</div>

	</div>
<?php endforeach;?>

</div>

<a href="<?= base_url();?>faq"><button class="faq-terms">FAQ &amp; TERMS</button></a>
<div class="member-line6"></div>
<a href="<?= base_url();?>profile"><button class="back-to-profile">Back To Profile</button></a>
</section>

<div class="cd-popup" id="sure" role="alert">
		<div class="cd-popup-container">
			<p>Are You Sure to Redeem ?</p>
			<div class="button-rdm">
				<a id="done"><span class="button-rdm-yes">YES</span></a>
				<a class="btn-close2" href="#0"><span class="button-rdm-no btn-close">NO</span></a>
			</div>
	<div class="rdm-warning">Please Note : This Act is For Cashier Only<br>
	to help get your redeem item</div>
	<!-- 		<ul class="cd-buttons">
				<li><a href="#0">Yes</a></li>
				<li><a href="#0">No</a></li>
			</ul> -->
			<!-- <a href="#0" class="cd-popup-close img-replace">Close</a> -->
		</div> <!-- cd-popup-container -->
</div>
<!-- <a href="#0" class="cd-popup-trigger">View Pop-up</a> -->
<!-- pop up redeem -->
<div class="cd-popup" id="not" role="alert">
		<div class="cd-popup-container">
			<p>Your Current Balance Point is</p>
			<div class="number-point not-enough margin-min"><?= $asd['point']?></div>
				<div class="rdm-warning not-enough margin-min2 padding-bot"><span class="more-point" id="kurang"></span> to Redeem This Item</div>
				<div class="ok-but"><a class="btn-close" href="#0"><span class="button-rdm-no btn-close">OK</span></a></div>
	<!-- 		<ul class="cd-buttons">
				<li><a href="#0">Yes</a></li>
				<li><a href="#0">No</a></li>
			</ul> -->
			<!-- <a href="#0" class="cd-popup-close img-replace">Close</a> -->
		</div> <!-- cd-popup-container -->
</div> <!-- cd-popup -->

		<footer>
			<div class="copyright-1">&copy; 2018 <span class="naniura-bold">NANIURA SUSHI</span> RESTAURANT</div>
			<div class="copyright-2">NANIURASUSHI.COM POWERED BY <a class="iobitdev-text" href="https://www.iobitdev.com/" target="_blank">IOBITDEV</a></div>
		</footer>

	</div>

<script type="text/javascript" src="<?= base_url();?>assets/js/swiper.min.js"></script>
<script type="text/javascript" src='<?= base_url();?>assets/js/iobitdev.js'></script>

<script type="text/javascript">
	jQuery(document).ready(function($){
	 var poin=0;
	//open popup enough point
	<?php foreach($voucher as $row):?>
	$('#redeem-btn<?= $row['id'];?>').on('click', function(event){
	    poin=<?= $row['point'];?>;							
	    event.preventDefault();
		$('#sure').addClass('is-visible');

				
	});
	<?php endforeach;?>
	$('#done').on('click', function(event){
	    redeem(poin);

				
	});
	//close popup
	$('.cd-popup').on('click', function(event){
		if( $(event.target).is('.btn-close') || $(event.target).is('.cd-popup') ) {
			event.preventDefault();
			$(this).removeClass('is-visible');
		}
	});

	//close popup
	$('.cd-popup').on('click', function(event){
		if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
			event.preventDefault();
			$(this).removeClass('is-visible');
		}
	});

	//close popup when clicking the esc keyboard button
	$(document).keyup(function(event){
    	if(event.which=='27'){
    		$('.cd-popup2').removeClass('is-visible');
	    }
    });

    function redeem(point){
        
		$('#sure').removeClass('is-visible');
         $.ajax({
		        url: '<?= base_url();?>point/redeem_point/'+point,
		        dataType: 'json',
		        success: function(data) {
					if(data>=0){
						window.location.href = "<?= base_url();?>redeem_done";

					}else{
							$('#kurang').html((data*-1)+" More Point");

							$('#not').addClass('is-visible');
					}
		        }

		    });
    }
   
});
</script>

</body>
</html>
