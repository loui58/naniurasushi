<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Naniura Sushi // Menu</title>
<meta name="Keywords" content="naniura,sushi,jakarta,sashimi,ramen,iobitdev,io,bit,dev,iobit">
<meta name="Description" content="Naniura in Batak Language is a Fish That is Cooked Without Cooking, This Fish is not Fried or Baked. Only the spices and the Sour of Jungga Acids (Citrus Jambhiri) are used to Ripen The Fish. The Fish is Marinated into The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten. The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten.">

<meta name="copyright" content="Naniura Sushi and naniurasushi.com 2017 All Rights Reserved Powered and Developed by IOBITDEV">
<meta name="author" content="IOBITDEV">
<meta name="robots" content="index,follow" />
<meta property="og:type" content="website" />
<meta property="og:image" content="<?=base_url();?>assets/img/img-share.jpg" />

<link rel="icon" href="<?=base_url();?>assets/img/favicon.png">

<link href="<?= base_url();?>assets/css/swiper.min.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/naniura.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/component.css" rel="stylesheet" type="text/css" >
<script src="<?= base_url();?>assets/js/jquery-latest.js"></script>

</head>

<body>
<div class="wrapper">

<!-- <div class="reg-pop">
	<div class="grab-text">grab some free gelato <span class="textbold">for naniura members</span></div>
	<div class="button-pop row-reg">
		<a href="register.html" target="_blank">
			<div class="pop-reg col-reg">register here</div>
		</a>
			<div class="pop-already col-reg">im already member :)</div>
	</div>
	<div class="button-regpop"><img src="img/cross.png" class="cross-pop" width="30"></div>
</div> -->

<!-- <div id="cover-regpop"></div> -->

<input type="checkbox" id="op"></input>
	<div class="lower">
	  <label for="op"><img src="<?=base_url();?>assets/img/menu-m.svg" width="36"></label>
	</div>

<div class="overlay overlay-hugeinc">
			<label for="op"></label>
			<nav class="navburger">
				<ul>
					<li class="navburger-logo"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m-black.svg" width="107"></a></li>
					<li><a href="<?= base_url();?>member">Member</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>bonbon"><span class="text-bonbon">Bon Bon Gelato</span></a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					<div class="menumobile-line"></div>
				</ul>
			</nav>

<div class="overlay-black"></div>
</div>

	<nav id="menu-fixed" class="topmenu-fix">
		<div class="mf-nav">
			<a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-white.svg" alt="Naniura Sushi" width="145" class="floatl"></a>
				<ul class="floatr">
					<li ><a href="<?= base_url();?>member">Member<span class="mf-badge"></span></a></li>
					<li class="li-active"><a href="<?= base_url();?>menu">Menu</a></li>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?=base_url();?>assets/img/bonbon-white.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
				</ul>
		</div>
		<div class="nav-m-fixed">
			<div class="brandlogo-m"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
	</nav>

	<header class="menu-header">
		<!--<div class="header-member"></div>-->
		<div class="wrapper-nav-m">
			<div class="brandlogo-m"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
			<div class="header-overlay">
				<a href="<?= base_url();?>"><img src="<?= base_url();?>assets/img/brandlogo.svg" alt="Naniura Sushi" width="150" class="brandlogo"></a>
				<nav class="topmenu cl-effect-4">
					<ul>
					<li><a href="<?= base_url();?>member">Member<span class="nav-badge"></span></a></li>
					<li class="cl-active"><a href="<?= base_url();?>menu">Menu</a></li>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?=base_url();?>assets/img/bonbon-logo.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					</ul>
				</nav>
				<!-- <div class="welcome-text">Best Place To Eat Sushi In<br>Our Neighbourhood</div>
				<a href="#"><button class="see-our-menu">SEE OUR MENU</button></a> -->
<!-- 				<div class="welcome-scroll">
					<a href="#title-menu">
				<div class="arrow-animate"><a href="#scrollmenu"><span></span></a></div>
				</a></div> -->
			</div>
		</header>
			<section class="featuredmenu" id="scrollmenu">
			<?php if($sushi!=null):?>

			<div class="title-menu">
				Sushi
				<div class="featured-line"></div>
				<div class="title-jp">寿司</div>
			</div>
			<?php $count=1;foreach($sushi as $row):

			if($count==1||($count%5==0&&$count>=5)):?>
			<div class="row">
			<?php endif;?>
				<div class="column">
					<div class="menu-wrapper">
						<a href="#">
						<div class="menu-thumbnail <?php if($row['cover']==''){
						echo ' menu-ph';
						}?>"><?php if($row['cover']==''){
						echo '<p style="margin-top:5px">'.str_replace(' ','<br/>',$row['name']).'</p>';
						}else{?><img src="<?= base_url();?><?= $row['cover']?>"><?php }?></div><?php if($row['fav']==1):?><span class="menu-stars"><img src="<?= base_url();?>assets/img/stars.png"></span><?php endif;?>
						<div class="menu-title"><?= $row['name']?></div>
						<div class="menu-line"></div>
						</a>
					</div>
				</div>
			<?php if((count($sushi)<4&&count($sushi)==$count)||($count%4==0&&$count>=4)):?>
			</div>
		<?php endif;$count++;?>
		<?php endforeach;?>
		</section>
	<?php endif;?>
		<section class="featuredmenu bg-menu-1" id="scrollmenu">
			<?php if($appetizer!=null):?>
			<div class="title-menu">
				Appetizers
				<div class="featured-line"></div>
				<div class="title-jp">前菜</div>
			</div>
			<?php $count=1;foreach($appetizer as $row):

			if($count==1||($count%5==0&&$count>=5)):?>
			<div class="row">
			<?php endif;?>
				<div class="column">
					<div class="menu-wrapper">
						<a href="#">
						<div class="menu-thumbnail <?php if($row['cover']==''){
						echo ' menu-ph';
						}?>"><?php if($row['cover']==''){
						echo '<p style="margin-top:5px">'.str_replace(' ','<br/>',$row['name']).'</p>';
						}else{?><img src="<?= base_url();?><?= $row['cover']?>"><?php }?></div><?php if($row['fav']==1):?><span class="menu-stars"><img src="<?= base_url();?>assets/img/stars.png"></span><?php endif;?>
						<div class="menu-title"><?= $row['name']?></div>
						<div class="menu-line"></div>
						</a>
					</div>
				</div>
			<?php if((count($appetizer)<4&&count($appetizer)==$count)||($count%4==0&&$count>=4)):?>
			</div>
		<?php endif;$count++;?>
		<?php endforeach;?>
			<div class="menu-spacer"></div>
		<?php endif;?>
		<?php if($soup!=null):?>
			<div class="title-menu">
				Soup &amp; Salad
				<div class="featured-line"></div>
				<div class="title-jp">スープとサラダ</div>
			</div>
			<?php $count=1;foreach($soup as $row):

			if($count==1||($count%5==0&&$count>=5)):?>
			<div class="row">
			<?php endif;?>
				<div class="column">
					<div class="menu-wrapper">
						<a href="#">
						<div class="menu-thumbnail <?php if($row['cover']==''){
						echo ' menu-ph';
						}?>"><?php if($row['cover']==''){
						echo '<p style="margin-top:5px">'.str_replace(' ','<br/>',$row['name']).'</p>';
						}else{?><img src="<?= base_url();?><?= $row['cover']?>"><?php }?></div><?php if($row['fav']==1):?><span class="menu-stars"><img src="<?= base_url();?>assets/img/stars.png"></span><?php endif;?>
						<div class="menu-title"><?= $row['name']?></div>
						<div class="menu-line"></div>
						</a>
					</div>
				</div>
			<?php if((count($soup)<4&&count($soup)==$count)||($count%4==0&&$count>=4)):?>
			</div>
		<?php endif;$count++;?>
		<?php endforeach;?>

		<?php endif;?>
		</section>
		<?php if($sashimi!=null):?>
		<section class="featuredmenu" id="scrollmenu">
			<div class="title-menu">
				Desserts
				<div class="featured-line"></div>
				<div class="title-jp">デザート</div>
			</div>
			<?php $count=1;foreach($sashimi as $row):

			if($count==1||($count%5==0&&$count>=5)):?>
			<div class="row">
			<?php endif;?>
				<div class="column">
					<div class="menu-wrapper">
						<a href="#">
						<div class="menu-thumbnail <?php if($row['cover']==''){
						echo ' menu-ph';
						}?>"><?php if($row['cover']==''){
						echo '<p style="margin-top:5px">'.str_replace(' ','<br/>',$row['name']).'</p>';
						}else{?><img src="<?= base_url();?><?= $row['cover']?>"><?php }?></div><?php if($row['fav']==1):?><span class="menu-stars"><img src="<?= base_url();?>assets/img/stars.png"></span><?php endif;?>
						<div class="menu-title"><?= $row['name']?></div>
						<div class="menu-line"></div>
						</a>
					</div>
				</div>
			<?php if((count($sashimi)<4&&count($sashimi)==$count)||($count%4==0&&$count>=4)):?>
			</div>
		<?php endif;$count++;?>
		<?php endforeach;?>
		</section>
	<?php endif;?>
	<section class="featuredmenu bg-menu-2" id="scrollmenu">

	<?php if($noodles!=null):?>
			<div class="title-menu">
				Noodles
				<div class="featured-line"></div>
				<div class="title-jp">麺類</div>
			</div>
			<?php $count=1;foreach($noodles as $row):

			if($count==1||($count%5==0&&$count>=5)):?>
			<div class="row">
			<?php endif;?>
				<div class="column">
					<div class="menu-wrapper">
						<a href="#">
						<div class="menu-thumbnail <?php if($row['cover']==''){
						echo ' menu-ph';
						}?>"><?php if($row['cover']==''){
						echo '<p style="margin-top:5px">'.str_replace(' ','<br/>',$row['name']).'</p>';
						}else{?><img src="<?= base_url();?><?= $row['cover']?>"><?php }?></div><?php if($row['fav']==1):?><span class="menu-stars"><img src="<?= base_url();?>assets/img/stars.png"></span><?php endif;?>
						<div class="menu-title"><?= $row['name']?></div>
						<div class="menu-line"></div>
						</a>
					</div>
				</div>
			<?php if((count($noodles)<4&&count($noodles)==$count)||($count%4==0&&$count>=4)):?>
			</div>
		<?php endif;$count++;?>
		<?php endforeach;?>
	<?php endif;?>
			<div class="menu-spacer"></div>
			<?php if($rice!=null):?>
			<div class="title-menu">
				Rice Bowl
				<div class="featured-line"></div>
				<div class="title-jp">ライスボウル</div>
			</div>
			<?php $count=1;foreach($rice as $row):

			if($count==1||($count%5==0&&$count>=5)):?>
			<div class="row">
			<?php endif;?>
				<div class="column">
					<div class="menu-wrapper">
						<a href="#">
						<div class="menu-thumbnail <?php if($row['cover']==''){
						echo ' menu-ph';
						}?>"><?php if($row['cover']==''){
						echo '<p style="margin-top:5px">'.str_replace(' ','<br/>',$row['name']).'</p>';
						}else{?><img src="<?= base_url();?><?= $row['cover']?>"><?php }?></div><?php if($row['fav']==1):?><span class="menu-stars"><img src="<?= base_url();?>assets/img/stars.png"></span><?php endif;?>
						<div class="menu-title"><?= $row['name']?></div>
						<div class="menu-line"></div>
						</a>
					</div>
				</div>
			<?php if((count($rice)<4&&count($rice)==$count)||($count%4==0&&$count>=4)):?>
			</div>
		<?php endif;$count++;?>
		<?php endforeach;?>
	<?php endif;?>
			<div class="download-menu-wrp">
				<a href="https://naniurasushi.com/download"><button class="download-menu">DOWNLOAD MENU PDF VERSION</button></a>
			</div>
		</section>

		<section class="sub-footer">
		<div class="sf-wrapper">
		<div class="row-sf">
			<div class="col-sf">
				<img src="<?= base_url();?>assets/img/naniura-white.svg" alt="Naniura" width="121">
					<div class="sf-tel"><a href="tel+6286611789">(+62) 866 117 89</a></div>
					<div class="socmed">
						<a href="facebook.html" target="_blank"><span class="soc-ico"><img src="<?= base_url();?>assets/img/fb.svg" alt="Facebook" width="37"></span></a>
						<a href="instagram.html" target="_blank"><span class="soc-ico"><img src="<?= base_url();?>assets/img/ig.svg" alt="Instagram" width="37"></span></a>
						<a href="twitter.html" target="_blank"><span class="soc-ico"><img src="<?= base_url();?>assets/img/tw.svg" alt="Twitter" width="37"></span></a>
					</div>
			</div>

			<div class="col-sf">
				<div class="sf-maps">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4880.036951269184!2d106.91220697221652!3d-6.248990026916265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e698cd3269f6585%3A0xe7f12a3eec82e5df!2sNani+Ura+Kalimalang!5e0!3m2!1sid!2sid!4v1511967762658" width="100%" height="147" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="sf-address">
					Tarum Barat, Kav. Agraria 6 E/5<br>Kalimalang, East Jakarta
				</div>
				<a href="https://www.google.com/maps/place/Nani+Ura+Kalimalang/@-6.2511345,106.9192893,13.81z/data=!4m5!3m4!1s0x0:0xe7f12a3eec82e5df!8m2!3d-6.2478867!4d106.9132627?hl=en-US" target="_blank">
				<button class="direction">DIRECTION</button>
				</a>
			</div>

			<div class="col-sf">
				<div class="openhours">
					<div class="sf-openhours-title">OPEN HOURS</div>
					<div class="sf-openhours-summary">
					Weekdays<br>10 AM - 09 PM
					</div>
					<div class="sf-openhours-summary">
					Thursday<br>11 AM - 10 PM
					</div>
					<div class="sf-openhours-summary">
					Weekend<br>10 AM - 10 PM
					</div>
				</div>
			</div>
		</div>
		</div>
		</section>


		<footer>
			<div class="copyright-1">&copy; 2018 <span class="naniura-bold">NANIURA SUSHI</span> RESTAURANT</div>
			<div class="copyright-2">NANIURASUSHI.COM POWERED BY <a class="iobitdev-text" href="https://www.iobitdev.com/" target="_blank">IOBITDEV</a></div>
		</footer>

	</div>

<script type="text/javascript" src="<?= base_url();?>assets/js/swiper.min.js"></script>
<script type="text/javascript" src='<?= base_url();?>assets/js/iobitdev.js'></script>

</body>
</html>
