<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Naniura Sushi // Profile</title>
<meta name="Keywords" content="naniura,sushi,jakarta,sashimi,ramen,iobitdev,io,bit,dev,iobit">
<meta name="Description" content="Naniura in Batak Language is a Fish That is Cooked Without Cooking, This Fish is not Fried or Baked. Only the spices and the Sour of Jungga Acids (Citrus Jambhiri) are used to Ripen The Fish. The Fish is Marinated into The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten. The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten.">

<meta name="copyright" content="Naniura Sushi and naniurasushi.com 2017 All Rights Reserved Powered and Developed by IOBITDEV">
<meta name="author" content="IOBITDEV">
<meta name="robots" content="index,follow" />
<meta property="og:type" content="website" />
<meta property="og:image" content="<?=base_url();?>assets/img/img-share.jpg" />

<link rel="icon" href="<?= base_url();?>assets/img/favicon.png">

<link href="<?= base_url();?>assets/css/swiper.min.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/naniura.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/component.css" rel="stylesheet" type="text/css" >
<script src="<?= base_url();?>assets/js/jquery-latest.js"></script>

</head>

<body class="wrapper-member">
<div class="wrapper">

<!-- <div class="reg-pop">
	<div class="grab-text">grab some free gelato <span class="textbold">for naniura members</span></div>
	<div class="button-pop row-reg">
		<a href="register.html" target="_blank">
			<div class="pop-reg col-reg">register here</div>
		</a>
			<div class="pop-already col-reg">im already member :)</div>
	</div>
	<div class="button-regpop"><img src="img/cross.png" class="cross-pop" width="30"></div>
</div> -->

<!-- <div id="cover-regpop"></div> -->

<input type="checkbox" id="op"></input>
	<div class="lower">
	  <label for="op"><img src="<?=base_url();?>assets/img/menu-m.svg" width="36"></label>
	</div>

<div class="overlay overlay-hugeinc">
			<label for="op"></label>
			<nav class="navburger">
				<ul>
					<li class="navburger-logo"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m-black.svg" width="107"></a></li>
					<li><a href="<?= base_url();?>member">Member</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>bonbon"><span class="text-bonbon">Bon Bon Gelato</span></a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					<div class="menumobile-line"></div>
				</ul>
			</nav>

<div class="overlay-black"></div>
</div>

	<nav id="menu-fixed" class="topmenu-fix">
		<div class="mf-nav">
			<a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-white.svg" alt="Naniura Sushi" width="145" class="floatl"></a>
				<ul class="floatr">
					<li class="li-active"><a href="<?= base_url();?>member">Member<span class="mf-badge"></span></a></li>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?=base_url();?>assets/img/bonbon-white.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
				</ul>
		</div>
		<div class="nav-m-fixed">
			<div class="brandlogo-m"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
	</nav>

	<header>
		<div class="header-member"></div>
		<div class="wrapper-nav-m">
			<div class="brandlogo-m"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
			<div class="header-overlay">
				<a href="<?= base_url();?>"><img src="<?= base_url();?>assets/img/brandlogo.svg" alt="Naniura Sushi" width="150" class="brandlogo"></a>
				<nav class="topmenu cl-effect-4">
					<ul>
					<li class="cl-active"><a href="<?= base_url();?>member">Member<span class="nav-badge"></span></a></li>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?=base_url();?>assets/img/bonbon-logo.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					</ul>
				</nav>
				<!-- <div class="welcome-text">Best Place To Eat Sushi In<br>Our Neighbourhood</div>
				<a href="#"><button class="see-our-menu">SEE OUR MENU</button></a> -->
<!-- 				<div class="welcome-scroll">
					<a href="#title-menu">
				<div class="arrow-animate"><a href="#scrollmenu"><span></span></a></div>
				</a></div> -->
			</div>
		</header>
<section class="member-page">
		<div class="title-member-wrapper">
			<div class="title-jp-big no-padding"><b>よくある質問</b></div>
				<div class="member-line"></div>
			<div class="title-member">Frequently Asked Question</div>
		</div>

		<div class="faq-paragraph-wrp">
			<b>Bagaimana cara melakukan registrasi?<br>
			How do I register my card?</b>
			<br><br>
			Untuk memulainya, login di www.naniurasushi.com/card. Pilih menu Registrasi. Siapkan Naniura Sushi Card Anda dan ikuti instruksi yang tertera. Semudah itu.
			 <br><br>
			<i>To get it started, log onto www.naniurasushi.com/card. Click the Registration menu. You’ll need your Card with you and then just follow the instructions to get your Card registered. It’s that easy! </i>
			 <br><br><br>
			<b>Apa saja keuntungan menggunakan Naniura Sushi Card?<br>
			What are the benefits of using a Naniura Sushi Card?</b>
			 <br><br>
			Manfaat pelanggan memiliki Specialtea Card antara lain adalah
			Pelanggan berhak mendapatkan point setiap pembelanjaan Rp. 5000 (lima ribu rupiah), berlaku kelipatan.
			Pelanggan bisa mendapatkan promo khusus seperti double point, harga spesial, hadiah  khusus di hari ulang tahun dan masih banyak lagi.
			Pelanggan mendapapatkan 1 free scoop bonbon gelato dengan menukarkan 100 point yang terdapat dalam SpecialTea Card.
			<br><br>
			<i>Benefits customers have Specialtea Card include:<br>
			Customers are entitled to a point of every Rp. 5000 (five thousand rupiah), multiples apply.
			Customers can get special promo like double point, special price, special gift on birthday and many more.
			Customers get 1 free scoop bonbon gelato by exchanging 50 points contained in SpecialTea Card.</i>
			<br><br><br><br>
			<b>Bagaimana cara menukar reward? <br>
			How do I redeem my rewards?</b>
			 <br><br>
			Katakan pada kasir kami jika Anda ingin menukar hadiah atau gunakan Naniura Sushi Card Anda dan kasir kami akan memberitahu jika Anda telah berhak menerima hadiah gratis.
			 <br><br>
			<i>Just tell our cashier you’d like to redeem your reward or simply use your registered Naniura Sushi Card and you will be notified by our cashier when you are eligible for a complimentary reward.</i>
			<br><br><br>
			<b>Bagaimana saya mengetahui jumlah point dalam Naniura Sushi Card?<br>
			How can I find out the point in my Naniura Sushi Card?</b>
			<br><br>
			Setelah kartu Anda teregistrasi, Anda dapat memeriksa point di kartu Anda dengan login ke www.naniurasushibar.com/card. Selain itu, point tersisa akan tercetak di struk transaksi Naniura Sushi Card.
			<br><br>
			<i>Once you have registered your card, you can check your point in your Card by logging onto www.naniurasushibar.com/card. Additionally, Naniura Sushi Card transaction receipts will include your remaining Card point.</i>
		</div>

<a href="<?= base_url();?>member"><button class="back-to-profile">Back</button></a>

</section>

		<footer>
			<div class="copyright-1">&copy; 2018 <span class="naniura-bold">NANIURA SUSHI</span> RESTAURANT</div>
			<div class="copyright-2">NANIURASUSHI.COM POWERED BY <a class="iobitdev-text" href="https://www.iobitdev.com/" target="_blank">IOBITDEV</a></div>
		</footer>

	</div>

<script type="text/javascript" src="<?= base_url();?>assets/js/swiper.min.js"></script>
<script type="text/javascript" src='<?= base_url();?>assets/js/iobitdev.js'></script>

</body>
</html>