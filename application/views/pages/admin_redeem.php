<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Naniura Sushi // Profile</title>
<meta name="Keywords" content="naniura,sushi,jakarta,sashimi,ramen,iobitdev,io,bit,dev,iobit">
<meta name="Description" content="Naniura in Batak Language is a Fish That is Cooked Without Cooking, This Fish is not Fried or Baked. Only the spices and the Sour of Jungga Acids (Citrus Jambhiri) are used to Ripen The Fish. The Fish is Marinated into The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten. The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten.">

<meta name="copyright" content="Naniura Sushi and naniurasushi.com 2017 All Rights Reserved Powered and Developed by IOBITDEV">
<meta name="author" content="IOBITDEV">
<meta name="robots" content="index,follow" />
<meta property="og:type" content="website" />
<meta property="og:image" content="<?=base_url();?>assets/img/img-share.jpg" />

<link rel="icon" href="<?=base_url();?>assets/img/favicon.png">

<link href="<?= base_url();?>assets/css/swiper.min.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/naniura.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/component.css" rel="stylesheet" type="text/css" >
<script src="<?= base_url();?>assets/js/jquery-latest.js"></script>
<script src="<?= base_url();?>assets/js/jquery.modal.min.js"></script>
<link rel="stylesheet" href="<?= base_url();?>assets/css/jquery.modal.min.css" />
<script type="text/javascript">
	jQuery(document).ready(function($){
	//open popup enough point
	$('#but-add-poin').on('click', function(event){
				var id=$("#memberid").val();
			    $("#but-add-poin").attr("disabled", true);
				$.ajax({
		        url: '<?= base_url();?>member/check_member/'+id,
		        dataType: 'json',
		        success: function(data) {
									if($('#point').val()==''){
										$("#point-modal-pur").modal({
										  fadeDuration: 1000,
										  fadeDelay: 0.50
										});

									}
									else if($('#point').val()<5000){
										$("#point-modal-kur").modal({
										  fadeDuration: 1000,
										  fadeDelay: 0.50
										});

									}
									else if(data==0){
										$("#point-modal").modal({
										  fadeDuration: 1000,
										  fadeDelay: 0.50
										});

									}else{

										$.ajax({
												url: '<?= base_url();?>point/add/'+id+'/'+($('#point').val()),
												dataType: 'json',
												success: function(data) {
													$("#point-modal-suc").modal({
													  fadeDuration: 1000,
													  fadeDelay: 0.50
													});

												}

										});
									}
									$("#but-add-poin").attr("disabled", false);
		        }

		    });
	});
});
</script>
</head>

<body class="wrapper-member">
<div class="wrapper">

<!-- <div class="reg-pop">
	<div class="grab-text">grab some free gelato <span class="textbold">for naniura members</span></div>
	<div class="button-pop row-reg">
		<a href="register.html" target="_blank">
			<div class="pop-reg col-reg">register here</div>
		</a>
			<div class="pop-already col-reg">im already member :)</div>
	</div>
	<div class="button-regpop"><img src="img/cross.png" class="cross-pop" width="30"></div>
</div> -->

<!-- <div id="cover-regpop"></div> -->

<input type="checkbox" id="op"></input>
	<div class="lower">
	  <label for="op"><img src="<?=base_url();?>assets/img/menu-m.svg" width="36"></label>
	</div>

<div class="overlay overlay-hugeinc">
			<label for="op"></label>
			<nav class="navburger">
				<ul>
					<li class="navburger-logo"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m-black.svg" width="107"></a></li>
					<li><a href="<?= base_url();?>member">Member</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>bonbon"><span class="text-bonbon">Bon Bon Gelato</span></a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					<div class="menumobile-line"></div>
				</ul>
			</nav>

<div class="overlay-black"></div>
</div>

	<nav id="menu-fixed" class="topmenu-fix">
		<div class="mf-nav">
			<a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-white.svg" alt="Naniura Sushi" width="145" class="floatl"></a>
				<ul class="floatr">
					<li class="li-active"><a href="<?= base_url();?>member">Member<span class="mf-badge"></span></a></li>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?=base_url();?>assets/img/bonbon-white.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
				</ul>
		</div>
		<div class="nav-m-fixed">
			<div class="brandlogo-m"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
	</nav>

	<header>
		<div class="header-member"></div>
		<div class="wrapper-nav-m">
			<div class="brandlogo-m"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
			<div class="header-overlay">
				<a href="<?= base_url();?>"><img src="<?= base_url();?>assets/img/brandlogo.svg" alt="Naniura Sushi" width="150" class="brandlogo"></a>
				<nav class="topmenu cl-effect-4">
					<ul>
					<li class="cl-active"><a href="<?= base_url();?>member">Member<span class="nav-badge"></span></a></li>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?=base_url();?>assets/img/bonbon-logo.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					</ul>
				</nav>
				<!-- <div class="welcome-text">Best Place To Eat Sushi In<br>Our Neighbourhood</div>
				<a href="#"><button class="see-our-menu">SEE OUR MENU</button></a> -->
<!-- 				<div class="welcome-scroll">
					<a href="#title-menu">
				<div class="arrow-animate"><a href="#scrollmenu"><span></span></a></div>
				</a></div> -->
			</div>
		</header>

<section class="member-page">
		<div class="title-member-wrapper">
			<div class="title-jp-big no-padding"><b>管理ページ</b></div>
				<div class="member-line"></div>
			<div class="title-member">Admin Page</div>

		</div>

		<div class="sign-in-section mrg-point">
			<div class="h1-point">scan barcode id / input member id<br>and total purchase here</div>
            <div class="form-add-point">
			    <input id="memberid" type="text" placeholder="enter member id" name="memberid" pattern="[0-9.]+" required style="margin-bottom: 10px;">
			    <input id="point" type="text" placeholder="and total purchase" name="purchase" pattern="[0-9.]+" required style="margin-bottom: 10px;">
			    <button id="but-add-poin">add point</button>
            </div>
		</div>
		<div class="cashier-logout"><a href="<?= base_url();?>member/logout"><button class="back-to-profile">Logout</button></a></div>
		<div class="member-line2"></div>
		<a href="<?= base_url();?>faq"><button class="why-join">FAQ For Member Can Read Here</button></a>
		

</section>

		<!-- Modal Point Added -->
<!-- 		  	<div id="point-modal" class="modal m-point">
			  <p>Point Added!</p>
			  <a href="addpoint.html" rel="modal:close">DONE</a>
			</div> -->

		<!-- Member ID Error -->
		  	<div id="point-modal" class="modal m-point not-enough">
			  <p>Error, Please Recheck Member ID</p>
			  <a href="#" rel="modal:close">ALRIGHT</a>
			</div>

				<div id="point-modal-pur" class="modal m-point not-enough">
					<p>Error, Please Recheck Total Purchase Value(s)</p>
					<a href="#" rel="modal:close">ALRIGHT</a>
				</div>
				<div id="point-modal-kur" class="modal m-point not-enough">
					<p>Error, Purchase Minimum 5000 for 1 Point</p>
					<a href="#" rel="modal:close">ALRIGHT</a>
				</div>

		  	<div id="point-modal-suc" class="modal m-point">
			  <p>Point Added!</p>
			  <a href="#" rel="modal:close">DONE</a>
			</div>

		<footer>
			<div class="copyright-1">&copy; 2018 <span class="naniura-bold">NANIURA SUSHI</span> RESTAURANT</div>
			<div class="copyright-2">NANIURASUSHI.COM POWERED BY <a class="iobitdev-text" href="https://www.iobitdev.com/" target="_blank">IOBITDEV</a></div>
		</footer>

	</div>

<script type="text/javascript" src="<?= base_url();?>assets/js/swiper.min.js"></script>
<script type="text/javascript" src='<?= base_url();?>assets/js/iobitdev.js'></script>

</body>
</html>
