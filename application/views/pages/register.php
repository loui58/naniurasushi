<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Naniura Sushi // Profile</title>
<meta name="Keywords" content="naniura,sushi,jakarta,sashimi,ramen,iobitdev,io,bit,dev,iobit">
<meta name="Description" content="Naniura in Batak Language is a Fish That is Cooked Without Cooking, This Fish is not Fried or Baked. Only the spices and the Sour of Jungga Acids (Citrus Jambhiri) are used to Ripen The Fish. The Fish is Marinated into The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten. The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten.">

<meta name="copyright" content="Naniura Sushi and naniurasushi.com 2017 All Rights Reserved Powered and Developed by IOBITDEV">
<meta name="author" content="IOBITDEV">
<meta name="robots" content="index,follow" />
<meta property="og:type" content="website" />
<meta property="og:image" content="<?=base_url();?>assets/img/img-share.jpg" />

<link rel="icon" href="<?= base_url();?>assets/img/favicon.png">

<link href="<?= base_url();?>assets/css/swiper.min.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/naniura.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/component.css" rel="stylesheet" type="text/css" >
<script src="<?= base_url();?>assets/js/jquery-latest.js"></script>

</head>

<body class="wrapper-member">
<div class="wrapper">

<input type="checkbox" id="op"></input>
	<div class="lower">
	  <label for="op"><img src="<?=base_url();?>assets/img/menu-m.svg" width="36"></label>
	</div>

<div class="overlay overlay-hugeinc">
			<label for="op"></label>
			<nav class="navburger">
				<ul>
					<li class="navburger-logo"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m-black.svg" width="107"></a></li>
					<li><a href="<?= base_url();?>member">Member</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>bonbon"><span class="text-bonbon">Bon Bon Gelato</span></a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					<div class="menumobile-line"></div>
				</ul>
			</nav>

<div class="overlay-black"></div>
</div>

	<nav id="menu-fixed" class="topmenu-fix">
		<div class="mf-nav">
			<a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-white.svg" alt="Naniura Sushi" width="145" class="floatl"></a>
				<ul class="floatr">
					<li class="li-active"><a href="<?= base_url();?>member">Member<span class="mf-badge"></span></a></li>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?=base_url();?>assets/img/bonbon-white.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
				</ul>
		</div>
		<div class="nav-m-fixed">
			<div class="brandlogo-m"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
	</nav>

	<header>
		<div class="header-member"></div>
		<div class="wrapper-nav-m">
			<div class="brandlogo-m"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
			<div class="header-overlay">
				<a href="<?= base_url();?>"><img src="<?= base_url();?>assets/img/brandlogo.svg" alt="Naniura Sushi" width="150" class="brandlogo"></a>
				<nav class="topmenu cl-effect-4">
					<ul>
					<li class="cl-active"><a href="<?= base_url();?>member">Member<span class="nav-badge"></span></a></li>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?=base_url();?>assets/img/bonbon-logo.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					</ul>
				</nav>
				<!-- <div class="welcome-text">Best Place To Eat Sushi In<br>Our Neighbourhood</div>
				<a href="#"><button class="see-our-menu">SEE OUR MENU</button></a> -->
<!-- 				<div class="welcome-scroll">
					<a href="#title-menu">
				<div class="arrow-animate"><a href="#scrollmenu"><span></span></a></div>
				</a></div> -->
			</div>
		</header>

<section class="member-page">
		<div class="title-member-wrapper">
			<div class="title-jp-big no-padding"><b>今すぐ登録</b></div>
				<div class="member-line"></div>
			<div class="title-member">Register Now</div>
			<?php echo validation_errors(); ?>

		</div>


		<div class="sign-in-section">
		<div class="avaform-wrp">


		<div class="form-wrp-1">
		<!-- sign up -->
		<form class="signup" method="post" action="register" enctype="multipart/form-data">

			<div id="pf_foto" style="background-image: url('<?= base_url();?>assets/img/def-ava.png');"></div>

			
		     <input type='file' id='verborgen_file' name="userfile"/>
    		<input type="button" value="change profile picture" id="uploadButton" />


		    <input class="inputreg" type="text" placeholder="Your Name" name="name" required>

		    <input class="inputreg" type="email" placeholder="Email" name="email" required>

		    <div class="form-dob">
		    	<div class="dob-text">Date of Birth</div>
							    	<div class="select-style sdate">
					<select name="day" id="day" onchange="setDays(month,this,year)">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
					</select>
					</div>
					<div class="select-style smonth">
					<select name="month" id="month" onchange="setDays(this,day,year)" placeholder="month">
						<option value="1">January</option>
						<option value="2">February</option>
						<option value="3">March</option>
						<option value="4">April</option>
						<option value="5">May</option>
						<option value="6">June</option>
						<option value="7">July</option>
						<option value="8">August</option>
						<option value="9">September</option>
						<option value="10">October</option>
						<option value="11">November</option>
						<option value="12">December</option>
					</select>
					</div>

					<div class="select-style syear">
					<select name="year" id="year" onchange="setDays(month,day,this)">
						<option value="1945">1945</option>
						<option value="1946">1946</option>
						<option value="1947">1947</option>
						<option value="1948">1948</option>
						<option value="1949">1949</option>
						<option value="1950">1950</option>
						<option value="1951">1951</option>
						<option value="1952">1952</option>
						<option value="1953">1953</option>
						<option value="1954">1954</option>
						<option value="1955">1955</option>
						<option value="1956">1956</option>
						<option value="1957">1957</option>
						<option value="1958">1958</option>
						<option value="1959">1959</option>
						<option value="1960">1960</option>
						<option value="1961">1961</option>
						<option value="1962">1962</option>
						<option value="1963">1963</option>
						<option value="1964">1964</option>
						<option value="1965">1965</option>
						<option value="1966">1966</option>
						<option value="1967">1967</option>
						<option value="1968">1968</option>
						<option value="1969">1969</option>
						<option value="1970">1970</option>
						<option value="1971">1971</option>
						<option value="1972">1972</option>
						<option value="1973">1973</option>
						<option value="1974">1974</option>
						<option value="1975">1975</option>
						<option value="1976">1976</option>
						<option value="1977">1977</option>
						<option value="1978">1978</option>
						<option value="1979">1979</option>
						<option value="1980">1980</option>
						<option value="1981">1981</option>
						<option value="1982">1982</option>
						<option value="1983">1983</option>
						<option value="1984">1984</option>
						<option value="1985">1985</option>
						<option value="1986">1986</option>
						<option value="1987">1987</option>
						<option value="1988">1988</option>
						<option value="1989">1989</option>
						<option value="1990">1990</option>
						<option value="1981">1991</option>
						<option value="1982">1992</option>
						<option value="1983">1993</option>
						<option value="1984">1994</option>
						<option value="1985">1995</option>
						<option value="1986">1996</option>
						<option value="1987">1997</option>
						<option value="1988">1998</option>
						<option value="1989">1999</option>
						<option value="1990">2000</option>
						<option value="1990">2001</option>
						<option value="1990">2002</option>
						<option value="1990">2003</option>
						<option value="1990">2004</option>
						<option value="1990">2005</option>
					</select>
					</div>
		    </div>

		    <input id="inp-mob" class="inputreg" type="text" placeholder="Mobile Phone" name="phone" required>

		    <input class="inputreg" type="password" placeholder="and Your Password Here" name="psw" required>

		    <button id="but-sign-up" type="submit">Register</button>

		</form>
		<!-- sign up -->

		    <div class="terms">register at naniurasushia.com means that you agree with terms and agreement</div>
<div class="member-line3"></div>
			<div class="button-reg-con2">
			    <a href="#" onclick="fb_login();" id="fb-root"><button class="button-fb-reg">or Connect With Facebook</button></a>
				

				<a href="<?= base_url();?>member"><button class="button-already">already member? sign in here :)</button></a>
			</div>

</div>
</div>
		</div>


</section>

		<footer>
			<div class="copyright-1">&copy; 2018 <span class="naniura-bold">NANIURA SUSHI</span> RESTAURANT</div>
			<div class="copyright-2">NANIURASUSHI.COM POWERED BY <a class="iobitdev-text" href="https://www.iobitdev.com/" target="_blank">IOBITDEV</a></div>
		</footer>

	</div>

<script type="text/javascript" src="<?= base_url();?>assets/js/swiper.min.js"></script>
<script type="text/javascript" src='<?= base_url();?>assets/js/iobitdev.js'></script>

<script type="text/javascript">

var numDays = {
                '1': 31, '2': 28, '3': 31, '4': 30, '5': 31, '6': 30,
                '7': 31, '8': 31, '9': 30, '10': 31, '11': 30, '12': 31
              };

function setDays(oMonthSel, oDaysSel, oYearSel)
{
	var nDays, oDaysSelLgth, opt, i = 1;
	nDays = numDays[oMonthSel[oMonthSel.selectedIndex].value];
	if (nDays == 28 && oYearSel[oYearSel.selectedIndex].value % 4 == 0)
		++nDays;
	oDaysSelLgth = oDaysSel.length;
	if (nDays != oDaysSelLgth)
	{
		if (nDays < oDaysSelLgth)
			oDaysSel.length = nDays;
		else for (i; i < nDays - oDaysSelLgth + 1; i++)
		{
			opt = new Option(oDaysSelLgth + i, oDaysSelLgth + i);
                  	oDaysSel.options[oDaysSel.length] = opt;
		}
	}
	var oForm = oMonthSel.form;
	var month = oMonthSel.options[oMonthSel.selectedIndex].value;
	var day = oDaysSel.options[oDaysSel.selectedIndex].value;
	var year = oYearSel.options[oYearSel.selectedIndex].value;
	oForm.hidden.value = month + '/' + day + '/' + year;
}

</script>
<script>
    window.fbAsyncInit = function() {
    FB.init({
        appId   : '162757597816500',
        oauth   : true,
        status  : true, // check login status
        cookie  : true, // enable cookies to allow the server to access the session
        xfbml   : true,
        version    : 'v2.11' 
    });

  };
function fb_login(){
    FB.login(function(response) {

        if (response.authResponse) {
            console.log('Welcome!  Fetching your information.... ');
            //console.log(response); // dump complete info
            access_token = response.authResponse.accessToken; //get access token
            user_id = response.authResponse.userID; //get FB UID

            FB.api('/me?locale=en_US&fields=id,name,email,first_name,last_name,birthday', function(response) {
                console.log(response)
                $.ajax({
                   type: "GET",
                   url: '<?= base_url();?>member/loginfb?id='+response.id+'&name='+response.name+'&email='+response.email+'&birthday='+response.birthday,
                   success: function(data)
                   {
                       window.location.replace("<?= base_url();?>member");
        
                   }
                });     
            });
        } else {
            //user hit cancel button
            console.log('User cancelled login or did not fully authorize.');

        }
    }, {
        scope: 'public_profile,email'
    });
}
(function() {
    var e = document.createElement('script');
    e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
    e.async = true;
    document.getElementById('fb-root').appendChild(e);
}());
</script>
<script type="text/javascript">
	$('#verborgen_file').hide();
        $('#uploadButton').on('click', function () {
              $('#verborgen_file').click();
        });

        $('#verborgen_file').change(function () {
            var file = this.files[0];
            var reader = new FileReader();
            reader.onloadend = function () {
               $('#pf_foto').css('background-image', 'url("' + reader.result + '")');
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
            }
        });
</script>
</body>
</html>