<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Naniura Sushi // Profile</title>
<meta name="Keywords" content="naniura,sushi,jakarta,sashimi,ramen,iobitdev,io,bit,dev,iobit">
<meta name="Description" content="Naniura in Batak Language is a Fish That is Cooked Without Cooking, This Fish is not Fried or Baked. Only the spices and the Sour of Jungga Acids (Citrus Jambhiri) are used to Ripen The Fish. The Fish is Marinated into The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten. The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten.">

<meta name="copyright" content="Naniura Sushi and naniurasushi.com 2017 All Rights Reserved Powered and Developed by IOBITDEV">
<meta name="author" content="IOBITDEV">
<meta name="robots" content="index,follow" />
<meta property="og:type" content="website" />
<meta property="og:image" content="<?=base_url();?>assets/img/img-share.jpg" />

<link rel="icon" href="<?=base_url();?>assets/img/favicon.png">

<link href="<?= base_url();?>assets/css/swiper.min.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/naniura.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/component.css" rel="stylesheet" type="text/css" >
<script src="<?= base_url();?>assets/js/jquery-latest.js"></script>

</head>
<body class="wrapper-member"  style='background-image: url("<?= base_url();?>assets/img/body-bg.jpg");'>
<div class="wrapper">

<!-- <div class="reg-pop">
	<div class="grab-text">grab some free gelato <span class="textbold">for naniura members</span></div>
	<div class="button-pop row-reg">
		<a href="register.html" target="_blank">
			<div class="pop-reg col-reg">register here</div>
		</a>
			<div class="pop-already col-reg">im already member :)</div>
	</div>
	<div class="button-regpop"><img src="img/cross.png" class="cross-pop" width="30"></div>
</div> -->

<!-- <div id="cover-regpop"></div> -->

<input type="checkbox" id="op"></input>
	<div class="lower">
	  <label for="op"><img src="<?=base_url();?>assets/img/menu-m.svg" width="36"></label>
	</div>

<div class="overlay overlay-hugeinc">
			<label for="op"></label>
			<nav class="navburger">
				<ul>
					<li class="navburger-logo"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m-black.svg" width="107"></a></li>
					<li><a href="<?= base_url();?>member">Member</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>bonbon"><span class="text-bonbon">Bon Bon Gelato</span></a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					<div class="menumobile-line"></div>
				</ul>
			</nav>

<div class="overlay-black"></div>
</div>

	<nav id="menu-fixed" class="topmenu-fix">
		<div class="mf-nav">
			<a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-white.svg" alt="Naniura Sushi" width="145" class="floatl"></a>
				<ul class="floatr">
					<li class="li-active"><a href="<?= base_url();?>member">Member<span class="mf-badge"></span></a></li>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?=base_url();?>assets/img/bonbon-white.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
				</ul>
		</div>
		<div class="nav-m-fixed">
			<div class="brandlogo-m"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
	</nav>

	<header>
		<div class="header-member"></div>
		<div class="wrapper-nav-m">
			<div class="brandlogo-m"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
			<div class="header-overlay">
				<a href="<?= base_url();?>"><img src="<?= base_url();?>assets/img/brandlogo.svg" alt="Naniura Sushi" width="150" class="brandlogo"></a>
				<nav class="topmenu cl-effect-4">
					<ul>
					<li class="cl-active"><a href="<?= base_url();?>member">Member<span class="nav-badge"></span></a></li>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?=base_url();?>assets/img/bonbon-logo.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					</ul>
				</nav>
				<!-- <div class="welcome-text">Best Place To Eat Sushi In<br>Our Neighbourhood</div>
				<a href="#"><button class="see-our-menu">SEE OUR MENU</button></a> -->
<!-- 				<div class="welcome-scroll">
					<a href="#title-menu">
				<div class="arrow-animate"><a href="#scrollmenu"><span></span></a></div>
				</a></div> -->
			</div>
		</header>
<section class="member-page">
		<div class="title-member-wrapper">
			<div class="title-jp-big"><b>参加する！</b></div>
				<div class="member-line"></div>
				<?php if(!empty($notactive)&&$notactive==1){?>
			<div class="title-member">Your Account Not Yet Activate</div>
			<div class="subtitle-member">Please Check Your Email to Activate Your Account</div>
				<?php }else{?>
			<div class="title-member">Lets Join at Naniura Sushi Membership</div>
			<div class="subtitle-member">get advantage such like promo and free gelato</div>
			<?php }?>
		</div>
		<?php if(empty($notactive)){?>
		<div class="first-time">
			<div class="h1-member">First Time Here?</div>
			<div class="button-reg-con">
				<a href="register"><button class="button-register">Register Now</button></a>
				<!--<a href="<?php echo $this->facebook->login_url(); ?>"><button class="button-fb">or Connect With Facebook</button></a>-->
				<a href="#" onclick="fb_login();" id="fb-root"><button class="button-fb">or Connect With Facebook</button></a>

                <?php /*
				<fb:login-button class="button-fb"length="long" size="large" 
                  scope="public_profile,email"
                  onlogin="checkLoginState();"><span style= "margin-right: 10px;size:20">or Connect With Facebook</span>
                </fb:login-button>*/?>
			</div>
		</div>
	<?php }?>
		<div class="sign-in-section">

			<?php if(empty($notactive)&&empty($wrong)){?>
			<div class="h1-member">nope, im already member :)</div>
		<?php }?>
		<?php if(!empty($wrong)){?>
		
			<div class="h1-member">sorry, wrong password or you not yet registered</div>
			<?php }?>
				<!-- sign in -->
				<form id="sign-in" method="post" action="<?= base_url();?>member/login">




		    <input id="sign-in-form" type="email" placeholder="enter your email" name="email" required>


		    <input id="sign-in-form" type="password" placeholder="and password" name="psw" required>

		    <button id="but-sign-in" type="submit">sign in</button>
		    <!-- <input type="checkbox" checked="checked"> Remember me -->



		    <div class="psw"><a href="<?= base_url();?>member/forgot">forgot your password?</a></div>

		</form>
				<!-- sign in -->
		</div>
		<div id="status">
</div>
		<div class="member-line2"></div>
		<a href="<?= base_url();?>faq"><button class="why-join">Why Join? See Benefits and FAQ Here</button></a>

</section>

		<footer>
			<div class="copyright-1">&copy; 2018 <span class="naniura-bold">NANIURA SUSHI</span> RESTAURANT</div>
			<div class="copyright-2">NANIURASUSHI.COM POWERED BY <a class="iobitdev-text" href="https://www.iobitdev.com/" target="_blank">IOBITDEV</a></div>
		</footer>

	</div>

<script type="text/javascript" src="<?= base_url();?>assets/js/swiper.min.js"></script>
<script type="text/javascript" src='<?= base_url();?>assets/js/iobitdev.js'></script>

<script>
    window.fbAsyncInit = function() {
    FB.init({
        appId   : '162757597816500',
        oauth   : true,
        status  : true, // check login status
        cookie  : true, // enable cookies to allow the server to access the session
        xfbml   : true,
        version    : 'v2.11' 
    });

  };
function fb_login(){
    FB.login(function(response) {

        if (response.authResponse) {
            console.log('Welcome!  Fetching your information.... ');
            //console.log(response); // dump complete info
            access_token = response.authResponse.accessToken; //get access token
            user_id = response.authResponse.userID; //get FB UID

            FB.api('/me?locale=en_US&fields=id,name,email,first_name,last_name,birthday', function(response) {
              
                $.ajax({
                   type: "GET",
                   url: '<?= base_url();?>member/loginfb?id='+response.id+'&name='+response.name+'&email='+response.email+'&birthday='+response.birthday,
                   success: function(data)
                   {
                       window.location.replace("<?= base_url();?>member");
        
                   }
                });     
            });
        } else {
            //user hit cancel button
            console.log('User cancelled login or did not fully authorize.');

        }
    }, {
        scope: 'public_profile,email'
    });
}
(function() {
    var e = document.createElement('script');
    e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
    e.async = true;
    document.getElementById('fb-root').appendChild(e);
}());
</script>
<?php /*
<script>
    //$( document ).ready(function() {
      function checkLoginState() {  
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
           
        });
      }
    //});
  function statusChangeCallback(response) {


    if (response.status === 'connected') {
      testAPI();
    } else {
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
        $.ajax({
           type: "GET",
           url: '<?= base_url();?>member/loginfb?id='+response.id+'&name='+response.name,
           success: function(data)
           {
               window.location.replace("<?= base_url();?>member");

           }
        });
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name +' '+response.id+ '!';
    });
  }
</script>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '162757597816500',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>*/?>

</body>
</html>
