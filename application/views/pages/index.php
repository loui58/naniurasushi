
	<header>
		<div class="header-bg"></div>
		<div class="wrapper-nav-m">
			<div class="brandlogo-m"><a href="index.html"><img src="<?= base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>

			<div class="header-overlay">
				<a href="<?= base_url();?>"><img src="<?= base_url();?>assets/img/brandlogo.svg" alt="Naniura Sushi" width="150" class="brandlogo"></a>
				<nav class="topmenu cl-effect-4">
					<ul>
					<li><a href="<?= base_url();?>member">Member<span class="nav-badge"></span></a></li>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?= base_url();?>assets/img/bonbon-logo.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					</ul>
				</nav>
				<div class="welcome-text">Best Place To Eat Sushi In<br>Our Neighbourhood</div>
				<a href="menu"><button class="see-our-menu">SEE OUR MENU</button></a>
				<div class="welcome-scroll">
					<a href="#title-menu">
				<div class="arrow-animate"><a href="#scrollmenu"><span></span></a></div>
				<!-- <img src="img/scroll.svg" alt="Scroll Down" width="25"> --></a></div>
			</div>
		</header>

		<section class="about">
			<div class="title-about">
				Naniura Sushi
				<div class="about-line"></div>
				<div class="title-jp">ナニウラ</div>
			</div>
			<div class="content-about">
				Naniura in Batak Language is a Fish That is Cooked Without Cooking, This Fish is not Fried or Baked. Only the spices and the Sour of Jungga Acids (Citrus Jambhiri) are used to Ripen The Fish. The Fish is Marinated into The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten. The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten.
			</div>
		</section>

		<section class="featuredmenu fmenupadbot" id="scrollmenu">
			<div class="title-menu">
				Featured Menu
				<div class="featured-line"></div>
				<div class="title-jp">メニューのメインステイ</div>
			</div>
			<div class="row">
				<?php foreach($menu as $row):?>
				<div class="column">
				<div class="menu-wrapper">
					
						<div class="menu-thumbnail <?php if($row['cover']==''){
						echo ' menu-ph';
						}?>"><?php if($row['cover']==''){
						echo '<p style="margin-top:5px">'.str_replace(' ','<br/>',$row['name']).'</p>';
						}else{?><img src="<?= base_url();?><?= $row['cover']?>"><?php }?></div><?php if($row['fav']==1):?><span class="menu-stars"><img src="<?= base_url();?>assets/img/stars.png"></span><?php endif;?>
						<div class="menu-title"><?= $row['name']?></div>
						<div class="menu-line"></div>
					
				</div>
				</div>
			<?php endforeach;?>
			</div>
			<a href="menu"><button class="all-menus">ALL MENUS<span class="right-arrow"><img src="<?= base_url();?>assets/img/right-arrow.svg" width="9"></span></button></a>
		</section>

		<section class="menu-gallery">
		<div class="swiper-slider">

  <div class="swiper-container">
    <div class="swiper-wrapper">
    <?php $no=1;foreach($slider as $s):?>
      <div class="swiper-slide" style="background: url(<?= base_url().$s['cover'];?>) no-repeat center; background-size: cover;">

        <a href="#<?= $no?>" class="swipe-bg">
        </a>

      </div>
    <?php endforeach;?>
  <!-- Add Arrows -->
<!--   <div class="swiper-button-next"></div>
  <div class="swiper-button-prev"></div> -->
  </div><!-- swiper-container-->
    <div class="swiper-pagination bulletbottom"></div>
</div> <!-- swiper-slider-->
<!-- 	<div class="title-works">Project : Revamp Okezone Welcome Page<br>
Client : MNC Media</div> -->
		</section>

		<section class="promoevent">
			<div class="title-about">
				Promo &amp; Event
				<div class="about-line"></div>
				<div class="title-jp">プロモーションとイベント</div>
			</div>
			
			<div class="row-2col">
				
				<div class="promolink col2"><a href="//naniurasushi.com/promo/detail/<?= $promo[0]['id'];?>"><img src="<?= base_url().$promo[0]['cover'];?>" class="imgpromo"></a></div>
				<div class="col2 alignleft">
					<a href="//naniurasushi.com/promo/detail/<?= $promo[0]['id'];?>">
					<div class="promolink promoevent-title">
						<?= $promo[0]['title']?>
					</div>
					<div class="promolink promoevent-summary">
						<?= $promo[0]['content']?>
					</div></a>
					<a href="promo"><button class="see-all alignleft">SEE ALL PROMOS<span class="right-arrow"></span></button></a>
				</div>
			</div></a>
		</section>

		<section class="subscribe">
			<div class="title-subscribe">
				<div class="title-subs-jp">ナニュラを手に入れよう！</div>
				<div class="title-subs-en">GET NANIURA!</div>
				<div class="summary-subs">receive our promotion, new menu<br>and updates from naniura sushi</div>
			</div>
			<form id="subscribe" method="post" class="subscribe-form">
      			<input type="email" name="email" id='email' class="subscribe-input" placeholder="YOUR MAIL ADDRESS" required>
      			<button type="submit" class="subscribe-submit">GET NOTIFY</button>
   			</form>
		</section>

		<!-- Modal -->
		  	<div id="subscribe-modal" class="modal">
			  <p>Thanks! Now You are Subscribed :)</p>
			  <a href="#" rel="modal:close">Okay</a>
			</div>

		<section class="sub-footer">
		<div class="sf-wrapper">
		<div class="row-sf">
			<div class="col-sf">
				<img src="<?= base_url();?>assets/img/naniura-white.svg" alt="Naniura" width="121">
					<div class="sf-tel"><a href="tel+6286611789">(+62) 866 117 89</a></div>
					<div class="socmed">
						<a href="facebook.html" target="_blank"><span class="soc-ico"><img src="<?= base_url();?>assets/img/fb.svg" alt="Facebook" width="37"></span></a>
						<a href="instagram.html" target="_blank"><span class="soc-ico"><img src="<?= base_url();?>assets/img/ig.svg" alt="Instagram" width="37"></span></a>
						<a href="twitter.html" target="_blank"><span class="soc-ico"><img src="<?= base_url();?>assets/img/tw.svg" alt="Twitter" width="37"></span></a>
					</div>
			</div>

			<div class="col-sf">
				<div class="sf-maps">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4880.036951269184!2d106.91220697221652!3d-6.248990026916265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e698cd3269f6585%3A0xe7f12a3eec82e5df!2sNani+Ura+Kalimalang!5e0!3m2!1sid!2sid!4v1511967762658" width="100%" height="147" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="sf-address">
					Tarum Barat, Kav. Agraria 6 E/5<br>Kalimalang, East Jakarta
				</div>
				<a href="https://www.google.com/maps/place/Nani+Ura+Kalimalang/@-6.2511345,106.9192893,13.81z/data=!4m5!3m4!1s0x0:0xe7f12a3eec82e5df!8m2!3d-6.2478867!4d106.9132627?hl=en-US" target="_blank">
				<button class="direction">DIRECTION</button>
				</a>
			</div>

			<div class="col-sf">
				<div class="openhours">
					<div class="sf-openhours-title">OPEN HOURS</div>
					<div class="sf-openhours-summary">
					Weekdays<br>10 AM - 09 PM
					</div>
					<div class="sf-openhours-summary">
					Thursday<br>11 AM - 10 PM
					</div>
					<div class="sf-openhours-summary">
					Weekend<br>10 AM - 10 PM
					</div>
				</div>
			</div>
		</div>
		</div>
		</section>

		<footer>
			<div class="copyright-1">&copy; 2018 <span class="naniura-bold">NANIURA SUSHI</span> RESTAURANT</div>
			<div class="copyright-2">NANIURASUSHI.COM POWERED BY <a class="iobitdev-text" href="https://www.iobitdev.com/" target="_blank">IOBITDEV</a></div>
		</footer>

	</div>

<script type="text/javascript" src="<?= base_url();?>assets/js/swiper.min.js"></script>
<script type="text/javascript" src='<?= base_url();?>assets/js/iobitdev.js'></script>

</body>
</html>
