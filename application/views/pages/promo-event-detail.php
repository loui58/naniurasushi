<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Naniura Sushi // Profile</title>
<meta name="Keywords" content="naniura,sushi,jakarta,sashimi,ramen,iobitdev,io,bit,dev,iobit">
<meta name="Description" content="Naniura in Batak Language is a Fish That is Cooked Without Cooking, This Fish is not Fried or Baked. Only the spices and the Sour of Jungga Acids (Citrus Jambhiri) are used to Ripen The Fish. The Fish is Marinated into The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten. The Seasoning That Can Chemically Transform Raw Fish into Cook Fish Which does not Taste Fishy and be Ready to be eaten.">

<meta name="copyright" content="Naniura Sushi and naniurasushi.com 2017 All Rights Reserved Powered and Developed by IOBITDEV">
<meta name="author" content="IOBITDEV">
<meta name="robots" content="index,follow" />
<meta property="fb:app_id" content="162757597816500" /> 
<meta property="og:url"  content="https:<?= base_url(uri_string());?>" />
<meta property="og:type"   content="website" />
<meta property="og:title"  content="<?= $promo[0]['title'];?>" /><?php
$text = strip_tags($promo[0]['content']);
$text = str_replace("&rsquo;","'", $text); 
$content = preg_replace("/&#?[a-z0-9]{2,8};/i","",$text );?>
<meta property="og:description"  content="<?= substr($text,0,250)?>" />
<?php if($promo[0]['cover']!=''){?>
<meta property="og:image" content="https:<?= str_replace(' ','_',base_url().$promo[0]['cover'])?>" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="315" />
<?php }?>
<link rel="canonical"
  href="<?= base_url(uri_string());?>">
<link rel="icon" href="<?= base_url();?>assets/img/favicon.png">

<link href="<?= base_url();?>assets/css/swiper.min.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/naniura.css" rel="stylesheet" type="text/css">
<link href="<?= base_url();?>assets/css/component.css" rel="stylesheet" type="text/css" >
<script src="<?= base_url();?>assets/js/jquery-latest.js"></script>

</head>

<body>
<div class="wrapper">

<!-- <div class="reg-pop">
	<div class="grab-text">grab some free gelato <span class="textbold">for naniura members</span></div>
	<div class="button-pop row-reg">
		<a href="register.html" target="_blank">
			<div class="pop-reg col-reg">register here</div>
		</a>
			<div class="pop-already col-reg">im already member :)</div>
	</div>
	<div class="button-regpop"><img src="img/cross.png" class="cross-pop" width="30"></div>
</div> -->

<!-- <div id="cover-regpop"></div> -->

<input type="checkbox" id="op"></input>
	<div class="lower">
	  <label for="op"><img src="<?=base_url();?>assets/img/menu-m.svg" width="36"></label>
	</div>

<div class="overlay overlay-hugeinc">
			<label for="op"></label>
			<nav class="navburger">
				<ul>
					<li class="navburger-logo"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m-black.svg" width="107"></a></li>
					<li><a href="<?= base_url();?>member">Member</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>bonbon"><span class="text-bonbon">Bon Bon Gelato</span></a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<div class="menumobile-line"></div>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					<div class="menumobile-line"></div>
				</ul>
			</nav>

<div class="overlay-black"></div>
</div>

	<nav id="menu-fixed" class="topmenu-fix">
		<div class="mf-nav">
			<a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-white.svg" alt="Naniura Sushi" width="145" class="floatl"></a>
				<ul class="floatr">
					<li><a href="<?= base_url();?>member">Member<span class="mf-badge"></span></a></li>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<li class="li-active"><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?=base_url();?>assets/img/bonbon-white.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
				</ul>
		</div>
		<div class="nav-m-fixed">
			<div class="brandlogo-m"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
	</nav>

	<header>
		<div class="header-member"></div>
		<div class="wrapper-nav-m">
			<div class="brandlogo-m"><a href="<?= base_url();?>"><img src="<?=base_url();?>assets/img/brandlogo-m.svg" width="107"></a></div>
			<!-- <div class="nav-m"><img src="img/menu-m.svg" width="36"></div> -->
		</div>
			<div class="header-overlay">
				<a href="<?= base_url();?>"><img src="<?= base_url();?>assets/img/brandlogo.svg" alt="Naniura Sushi" width="150" class="brandlogo"></a>
				<nav class="topmenu cl-effect-4">
					<ul>
					<li ><a href="<?= base_url();?>member">Member<span class="nav-badge"></span></a></li>
					<li><a href="<?= base_url();?>menu">Menu</a></li>
					<li class="cl-active"><a href="<?= base_url();?>promo">Promo &amp; Event</a></li>
					<li class="bonbon-nav"><a href="<?= base_url();?>bonbon"><img src="<?=base_url();?>assets/img/bonbon-logo.svg" alt="Bon Bon Gelato" width="75" class="vertical-al"></a></li>
					<li><a href="<?= base_url();?>shop">Shop</a></li>
					<li><a href="<?= base_url();?>location">Location &amp; Contact</a></li>
					</ul>
				</nav>
				<!-- <div class="welcome-text">Best Place To Eat Sushi In<br>Our Neighbourhood</div>
				<a href="#"><button class="see-our-menu">SEE OUR MENU</button></a> -->
<!-- 				<div class="welcome-scroll">
					<a href="#title-menu">
				<div class="arrow-animate"><a href="#scrollmenu"><span></span></a></div>
				</a></div> -->
			</div>
		</header>

<section class="member-detail-page">
		<div class="detail-wrapper">
			<?php foreach($promo as $row):?>
			<div class="detail-line"></div>

			<div class="title-cat-time-wrp">
				<div class="title-details"><?= $row['title'];?></div>
				<div class="cat-time-details"><span class="cat-detail">PROMO</span> - <span class="time-detail"><?= $row['ago'];?></span>
				</div>
			</div>
			<div class="image-details" style="background-image:url('<?= base_url().$row['cover'];?>')"></div>
			<article>
					<?= $row['content']?>

			</article>
			<div class="share-it-wrp">Share it
				<div class="share-it-icon"><a id="fb-share" href="http://www.facebook.com/sharer.php?u=<?= 'https:'.base_url(uri_string());?>" target="_blank"><span class="fb-share" ><img src="<?= base_url();?>assets/img/fb-share.svg" alt="Share on Facebook" width="12"></span></a><span class="v-separator"></span><a href="https://twitter.com/home?<?= urlencode(base_url(uri_string()));?>" id="twitter-share" target="_blank"><span class="tw-share"><img src="<?= base_url();?>assets/img/tw-share.svg" alt="Share on Twitter" width="28"></span></a>
				</div>
			</div>
		<?php endforeach;?>
		</div>
</section>

<section class="other-promotion">
	<div class="oth-promo-wrp">
		<div class="title-oth-wrp">
			<div class="title-oth">Other Promotion &amp; Event</div>
			<div class="detail-line2"></div>
			<div class="title-oth-jp">その他のプロモーションやイベント</div>
		</div>

		<div class="row-3">
		    <?php foreach($other as $row){?>
				<div class="column-3"><a href="//naniurasushi.com/promo/detail/<?= $row['id'];?>">
				<div class="article-wrp">
						<div class="article-thumb" style="background-image: url('<?= base_url().$row['cover'];?>')"></div>
						<div class="article-summary"><?= $row['title'];?></div>
				</div></a>
				</div>
				<?php }?>

		</div>
	</div>
</section>

		<section class="subscribe">
			<div class="title-subscribe">
				<div class="title-subs-jp">ナニュラを手に入れよう！</div>
				<div class="title-subs-en">GET NANIURA!</div>
				<div class="summary-subs">receive our promotion, new menu<br>and updates from naniura sushi</div>
			</div>
			<form id="subscribe" method="post" class="subscribe-form">
      			<input type="email" name="email" class="subscribe-input" placeholder="YOUR MAIL ADDRESS" required>
      			<button type="submit" class="subscribe-submit">GET NOTIFY</button>
   			</form>
		</section>

		<!-- Modal -->
		  	<div id="subscribe-modal" class="modal">
			  <p>Thanks! Now You are Subscribed :)</p>
			  <a href="#" rel="modal:close">Okay</a>
			</div>

		<section class="sub-footer">
		<div class="sf-wrapper">
		<div class="row-sf">
			<div class="col-sf">
				<img src="<?= base_url();?>assets/img/naniura-white.svg" alt="Naniura" width="121">
					<div class="sf-tel"><a href="tel+6286611789">(+62) 866 117 89</a></div>
					<div class="socmed">
						<a href="facebook.html" target="_blank"><span class="soc-ico"><img src="<?= base_url();?>assets/img/fb.svg" alt="Facebook" width="37"></span></a>
						<a href="instagram.html" target="_blank"><span class="soc-ico"><img src="<?= base_url();?>assets/img/ig.svg" alt="Instagram" width="37"></span></a>
						<a href="twitter.html" target="_blank"><span class="soc-ico"><img src="<?= base_url();?>assets/img/tw.svg" alt="Twitter" width="37"></span></a>
					</div>
			</div>

			<div class="col-sf">
				<div class="sf-maps">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4880.036951269184!2d106.91220697221652!3d-6.248990026916265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e698cd3269f6585%3A0xe7f12a3eec82e5df!2sNani+Ura+Kalimalang!5e0!3m2!1sid!2sid!4v1511967762658" width="100%" height="147" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="sf-address">
					Tarum Barat, Kav. Agraria 6 E/5<br>Kalimalang, East Jakarta
				</div>
				<a href="https://www.google.com/maps/place/Nani+Ura+Kalimalang/@-6.2511345,106.9192893,13.81z/data=!4m5!3m4!1s0x0:0xe7f12a3eec82e5df!8m2!3d-6.2478867!4d106.9132627?hl=en-US" target="_blank">
				<button class="direction">DIRECTION</button>
				</a>
			</div>

			<div class="col-sf">
				<div class="openhours">
					<div class="sf-openhours-title">OPEN HOURS</div>
					<div class="sf-openhours-summary">
					Weekdays<br>10 AM - 09 PM
					</div>
					<div class="sf-openhours-summary">
					Thursday<br>11 AM - 10 PM
					</div>
					<div class="sf-openhours-summary">
					Weekend<br>10 AM - 10 PM
					</div>
				</div>
			</div>
		</div>
		</div>
		</section>

		<footer>
			<div class="copyright-1">&copy; 2018 <span class="naniura-bold">NANIURA SUSHI</span> RESTAURANT</div>
			<div class="copyright-2">NANIURASUSHI.COM POWERED BY <a class="iobitdev-text" href="https://www.iobitdev.com/" target="_blank">IOBITDEV</a></div>
		</footer>

	</div>

<script type="text/javascript" src="<?= base_url();?>assets/js/swiper.min.js"></script>
<script type="text/javascript" src='<?= base_url();?>assets/js/iobitdev.js'></script>
    <script>
        $(document).ready(function() {
    $('.fb-share').click(function(e) {
        e.preventDefault();
        window.open('http://www.facebook.com/sharer.php?u=<?= 'http:'.base_url(uri_string());?>', 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        return false;
    });
     $('#twitter-share').click(function(e) {
        e.preventDefault();
        window.open('http://twitter.com/intent/tweet?url=<?= urlencode(base_url(uri_string()));?>;text=<?= urlencode(substr($promo[0]['content'],0,15))?>;hashtags=naniurasushi', 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        return false;
    });
    
});
    </script>
    
    
    
</body>
</html>
