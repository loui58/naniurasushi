<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<!-- Mirrored from getbootstrapadmin.com/remark/material/base/tables/datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Nov 2017 08:37:34 GMT -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">

  <title>Naniura Content Management System</title>

  <link rel="apple-touch-icon" href="<?= base_url();?>assets//images/apple-touch-icon.png">
  <link rel="shortcut icon" href="<?= base_url();?>assets//images/favicon.ico">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/css/bootstrap.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/css/bootstrap-extend.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets//css/site.min3f0d.css?v2.2.0">

  <!-- Skin tools (demo site only) -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/css/skintools.min3f0d.css?v2.2.0">
  <script src="<?= base_url();?>assets//js/sections/skintools.min.js"></script>

  <!-- Plugins -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/animsition/animsition.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/asscrollable/asScrollable.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/switchery/switchery.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/intro-js/introjs.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/slidepanel/slidePanel.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/flag-icon-css/flag-icon.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/waves/waves.min3f0d.css?v2.2.0">

  <!-- Plugins For This Page -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/vendor/datatables-responsive/dataTables.responsive.min3f0d.css?v2.2.0">

  <!-- Page -->
  <link rel="stylesheet" href="<?= base_url();?>assets//examples/css/tables/datatable.min3f0d.css?v2.2.0">

  <!-- Fonts -->
  <link rel="stylesheet" href="<?= base_url();?>assets/global/fonts/material-design/material-design.min3f0d.css?v2.2.0">
  <link rel="stylesheet" href="<?= base_url();?>assets/global/fonts/brand-icons/brand-icons.min3f0d.css?v2.2.0">

  <link rel='stylesheet' href='<?= base_url();?>assets/<?= base_url();?>assets/<?= base_url();?>assets/fonts.googleapis.com/css5478.css?family=Roboto:400,400italic,700'>

  <link rel="stylesheet" href="<?= base_url();?>assets/global/fonts/font-awesome/font-awesome.min3f0d.css?v2.2.0">

  <!--[if lt IE 9]>
    <script src="<?= base_url();?>assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->

  <!--[if lt IE 10]>
    <script src="<?= base_url();?>assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="<?= base_url();?>assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->

  <!-- Scripts -->
  <script src="<?= base_url();?>assets/global/vendor/modernizr/modernizr.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/breakpoints/breakpoints.min.js"></script>
  <script>
    Breakpoints();
  </script>
</head>
<body>
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

  <nav class="site-navbar navbar navbar-inverse navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
      data-toggle="menubar">
        <span class="sr-only">Toggle navigation</span>
        <span class="hamburger-bar"></span>
      </button>
      <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
      data-toggle="collapse">
        <i class="icon md-more" aria-hidden="true"></i>
      </button>
      <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" >
        <!--<img class="navbar-brand-logo" src="<?= base_url();?>assets//images/logo.png" title="Remark">-->
        <span class="navbar-brand-text hidden-xs"> Naniura</span>
      </div>
      <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-search"
      data-toggle="collapse">
        <span class="sr-only">Toggle Search</span>
        <i class="icon md-search" aria-hidden="true"></i>
      </button>
    </div>

    <div class="navbar-container container-fluid">
      <!-- Navbar Collapse -->
      <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
        <!-- Navbar Toolbar -->
        <ul class="nav navbar-toolbar">
          
        </ul>
        <!-- End Navbar Toolbar -->

        <!-- Navbar Toolbar Right -->
       
        <!-- End Navbar Toolbar Right -->
      </div>
      <!-- End Navbar Collapse -->

      <!-- Site Navbar Seach -->
      <div class="collapse navbar-search-overlap" id="site-navbar-search">
        <form role="search">
          <div class="form-group">
            <div class="input-search">
              <i class="input-search-icon md-search" aria-hidden="true"></i>
              <input type="text" class="form-control" name="site-search" placeholder="Search...">
              <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
              data-toggle="collapse" aria-label="Close"></button>
            </div>
          </div>
        </form>
      </div>
      <!-- End Site Navbar Seach -->
    </div>
  </nav>
  <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu">
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>promo/manage">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Promo & Event</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>menu/manage">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Menu</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>voucher/manage">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Voucher</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>member/manage">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Member</span>
              </a>
            </li>
            <li class="site-menu-item">
              <a class="animsition-link" href="<?= base_url();?>slider">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Slider</span>
              </a>
            </li>
          </div>
        </div>
      </div>
    </div>

    <div class="site-menubar-footer">
    
      <a href="<?= base_url();?>member/logout" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
        <span class="icon md-power" aria-hidden="true"></span>
      </a>
    </div>
  </div>
  <div class="site-gridmenu">
    <div>
      <div>
        <ul>
          
        </ul>
      </div>
    </div>
  </div>
