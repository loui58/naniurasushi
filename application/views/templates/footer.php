

  <!-- Footer -->
  <footer class="site-footer">
    <div class="site-footer-legal">© 2017</div>
    <div class="site-footer-right">
      Crafted with <i class="red-600 icon md-favorite"></i> by WAR.Tech
    </div>
  </footer>
  <!-- Core  -->
  <script src="<?= base_url();?>assets/global/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/animsition/animsition.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asscroll/jquery-asScroll.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/mousewheel/jquery.mousewheel.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asscrollable/jquery.asScrollable.all.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/ashoverscroll/jquery-asHoverScroll.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/waves/waves.min.js"></script>

  <!-- Plugins -->
  <script src="<?= base_url();?>assets/global/vendor/switchery/switchery.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/intro-js/intro.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/screenfull/screenfull.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/slidepanel/jquery-slidePanel.min.js"></script>

  <!-- Scripts -->
  <script src="<?= base_url();?>assets/global/js/core.min.js"></script>
  <script src="<?= base_url();?>assets/js/site.min.js"></script>

  <script src="<?= base_url();?>assets/js/sections/menu.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/menubar.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/gridmenu.min.js"></script>
  <script src="<?= base_url();?>assets/js/sections/sidebar.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/configs/config-colors.min.js"></script>
  <script src="<?= base_url();?>assets/js/configs/config-tour.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/components/asscrollable.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/animsition.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/slidepanel.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/switchery.min.js"></script>
  <script src="<?= base_url();?>assets/global/js/components/tabs.min.js"></script>

  <script src="<?= base_url();?>assets/global/js/components/datatables.min.js"></script>

  <script src="<?= base_url();?>assets/examples/js/tables/datatable.min.js"></script>
  <script src="<?= base_url();?>assets/examples/js/uikit/icon.min.js"></script>

  <script src="<?= base_url();?>assets/examples/js/uikit/dropdown.min.js"></script>
  <script src="<?= base_url();?>global/vendor/jquery-placeholder/jquery.placeholder.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/datatables-responsive/dataTables.responsive.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/asrange/jquery-asRange.min.js"></script>
  <script src="<?= base_url();?>assets/global/vendor/bootbox/bootbox.js"></script>
  <script>
  $("#addbutton").click(function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: "<?= base_url();?>cart/add/",
        data: {
            item: $("#item").val(), // < note use of 'this' here
            jumlah: $("#jumlah").val()
        },
        success: function(result) {
          $('#exampleGrid').modal('toggle');

        },
        error: function(result) {
        }
    });
  });

  </script>
  <!-- Google Analytics -->

</body>


<!-- Mirrored from getbootstrapadmin.com/remark/material/base/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Nov 2017 08:17:01 GMT -->
</html>
